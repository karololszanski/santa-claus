import {
  Entity,
  BaseEntity,
  PrimaryColumn,
  CreateDateColumn,
  Column,
} from "typeorm";
import { IsString, IsJWT } from "class-validator";

@Entity()
export class Token extends BaseEntity {
  @PrimaryColumn({
    unique: true,
  })
  @IsString()
  @IsJWT()
  refresh_token: string;

  @Column({
    unique: true,
  })
  @IsString()
  @IsJWT()
  access_token: string;

  @CreateDateColumn()
  created_at: Date;
}
