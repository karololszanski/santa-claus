import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm";
import { Letter } from "./Letter";
import {
  IsString,
  Length,
  IsPostalCode,
  IsEmail,
  IsPhoneNumber,
  IsLatitude,
  IsLongitude,
  IsEnum,
} from "class-validator";

export enum Voivodeshipes {
  DOLNOSLASKIE = "dolnośląskie",
  KUJAWSKOPOMORSKIE = "kujawsko-pomorskie",
  LUBELSKIE = "lubelskie",
  LUBUSKIE = "lubuskie",
  LODZKIE = "łódzkie",
  MALOPOLSKIE = "małopolskie",
  MAZOWIECKIE = "mazowieckie",
  OPOLSKIE = "opolskie",
  PODKARPACKIE = "podkarpackie",
  PODLASKIE = "podlaskie",
  POMORSKIE = "pomorskie",
  SLASKIE = "śląskie",
  SWIETOKRZYSKIE = "świętokrzyskie",
  WARMINSKOMAZURSKIE = "warmińsko-mazurskie",
  WIELKOPOLSKIE = "wielkopolskie",
  ZACHODNIOPOMORSKIE = "zachodniopomorskie",
}

@Entity()
export class Orphanage extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column({
    length: 64,
  })
  @IsString()
  @Length(2, 64)
  name: string;

  @Column({
    unique: true,
  })
  @IsString()
  @IsEmail()
  email: string;

  @Column()
  @IsString()
  password: string;

  @Column({
    unique: true,
    length: 6,
  })
  @IsString()
  @Length(6, 6)
  code: string;

  @Column()
  @IsString()
  @IsPhoneNumber("PL")
  phone: string;

  @Column({
    type: "enum",
    enum: Voivodeshipes,
    nullable: true,
  })
  @IsEnum(Voivodeshipes)
  voivodeship: Voivodeshipes;

  @Column()
  @IsString()
  @IsPostalCode("PL")
  postal_code: string;

  @Column()
  @IsString()
  city: string;

  @Column()
  @IsString()
  street: string;

  @Column()
  @IsString()
  building_number: string;

  @Column()
  @IsLatitude()
  lat: string;

  @Column()
  @IsLongitude()
  lng: string;

  @OneToMany(() => Letter, (letter) => letter.orphanage, {
    onDelete: "SET NULL",
  })
  letters: Letter[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
