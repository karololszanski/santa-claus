import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { Category } from "./Category";
import { Orphanage } from "./Orphanage";
import { User } from "./User";
import { IsString, Length, Min, Max, IsInt } from "class-validator";

@Entity()
export class Letter extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column({
    length: 64,
  })
  @IsString()
  @Length(2, 64)
  first_name: string;

  @Column({
    length: 64,
  })
  @IsString()
  @Length(2, 64)
  last_name: string;

  @Column({
    length: 1500,
  })
  @IsString()
  @Length(50, 1500)
  text: string;

  @Column({
    default: 1,
  })
  @IsInt()
  @Min(0)
  @Max(5)
  step: number;

  @ManyToOne(() => User, (user) => user.letters, {
    onDelete: "SET NULL",
    nullable: true,
  })
  user: User;

  @ManyToOne(() => Orphanage, (orphanage) => orphanage.letters, {
    onDelete: "CASCADE",
    nullable: false,
  })
  @JoinColumn()
  orphanage: Orphanage;

  @ManyToOne(() => Category, (category) => category.letters, {
    onDelete: "SET NULL",
    nullable: true,
  })
  category: Category;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
