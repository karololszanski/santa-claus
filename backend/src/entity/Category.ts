import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from "typeorm";
import { Letter } from "./Letter";
import { IsString, Length } from "class-validator";

@Entity()
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column()
  @IsString()
  @Length(2, 64)
  name: string;

  @Column()
  @IsString()
  @Length(2, 64)
  parent: string;

  @OneToMany(() => Letter, (letter) => letter.user)
  letters: Letter[];
}
