import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm";
import { Letter } from "./Letter";
import { IsString, Length, IsEmail, IsPhoneNumber } from "class-validator";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  uuid: string;

  @Column({
    length: 64,
  })
  @IsString()
  @Length(2, 64)
  first_name: string;

  @Column({
    length: 64,
  })
  @IsString()
  @Length(2, 64)
  last_name: string;

  @Column({
    unique: true,
  })
  @IsString()
  @IsEmail()
  @Length(2, 64)
  email: string;

  @Column()
  @IsString()
  password: string;

  @Column({
    unique: true,
  })
  @IsString()
  @IsPhoneNumber("PL")
  phone: string;

  @OneToMany(() => Letter, (letter) => letter.user, {
    nullable: true,
    onDelete: "SET NULL",
  })
  letters: Letter[];

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
