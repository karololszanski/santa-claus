import * as dotenv from "dotenv";
import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import { createUserRouter } from "./routes/create_user";
import { createLetterRouter } from "./routes/create_letter";
import { connectLetterToUserRouter } from "./routes/connect_letter_to_user";
import { connectCategoryToLetterRouter } from "./routes/connect_category_to_letter";
import { updateLetterStepRouter } from "./routes/update_letter_step";
import { getCategoriesRouter } from "./routes/get_categories";
import { getLettersRouter } from "./routes/get_letters";
import { loginUserRouter } from "./routes/login_user";
import { loginOrphanageRouter } from "./routes/login_orphanage";
import { logoutRouter } from "./routes/logout";
import { newTokenRouter } from "./routes/newToken";
import { getAllLettersRouter } from "./routes/get_all_letters";
import cors from "cors";
import { checkOrphanageRouter } from "./routes/check_orphanage_code";
import { getMyLettersRouter } from "./routes/get_my_letters";

dotenv.config();
const app = express();
const isProduction = process.env.NODE_ENV === "production";

createConnection(
  isProduction
    ? {
        type: "postgres",
        url: (process.env.DATABASE_URL as string) ?? undefined,
        synchronize: true,
        logging: false,
        entities: ["src/entity/**/*.ts"],
        migrations: ["src/migration/**/*.ts"],
        subscribers: ["src/subscriber/**/*.ts"],
        cli: {
          entitiesDir: "src/entity",
          migrationsDir: "src/migration",
          subscribersDir: "src/subscriber",
        },
        ssl: true,
        extra: {
          ssl: {
            rejectUnauthorized: false,
          },
        },
      }
    : {
        type: "postgres",
        host: "localhost",
        port: parseInt(<string>process.env.DATABASE_PORT) | 5432,
        username: process.env.USERNAME,
        password: process.env.PASSWORD,
        database: "santa-claus",
        synchronize: true,
        logging: false,
        entities: ["src/entity/**/*.ts"],
        migrations: ["src/migration/**/*.ts"],
        subscribers: ["src/subscriber/**/*.ts"],
        cli: {
          entitiesDir: "src/entity",
          migrationsDir: "src/migration",
          subscribersDir: "src/subscriber",
        },
      }
)
  .then(async (connection) => {
    console.log("Connected to Postgres");

    app.use(express.json());
    app.use(cors());

    app.use(createUserRouter);
    app.use(createLetterRouter);
    app.use(connectLetterToUserRouter);
    app.use(connectCategoryToLetterRouter);
    app.use(updateLetterStepRouter);
    app.use(getCategoriesRouter);
    app.use(getAllLettersRouter);
    app.use(getLettersRouter);
    app.use(getMyLettersRouter);
    app.use(loginUserRouter);
    app.use(loginOrphanageRouter);
    app.use(logoutRouter);
    app.use(newTokenRouter);
    app.use(checkOrphanageRouter);

    app.listen(process.env.PORT || 8080, () => {
      console.log(`Now running on port ${process.env.PORT || 8080}`);
    });
  })
  .catch((error) => {
    console.log("Unable to connect to Postgres: ", error);
    throw new Error("Unable to connect to Postgres");
  });
