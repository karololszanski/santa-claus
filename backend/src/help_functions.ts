import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { Token } from "./entity/Token";

export function generateAccessToken(object: any) {
  return jwt.sign(object, process.env.ACCESS_TOKEN_SECRET as string, {
    expiresIn: "15s",
  });
}

export function generateRefreshToken(object: any) {
  return jwt.sign(object, process.env.REFRESH_TOKEN_SECRET as string);
}

export function authenticateToken(req: any, res: any, next: any) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.status(401).json({ msg: "Unauthorized" });

  jwt.verify(
    token,
    process.env.ACCESS_TOKEN_SECRET as string,
    async (err: any, object: any) => {
      console.log(err);
      if (err) return res.status(401).json({ msg: "Unauthorized" });
      const tokenRetrieved = await getRepository(Token).findOne({
        where: { access_token: token },
      });
      console.log("Token: ", tokenRetrieved);
      if (!tokenRetrieved || typeof tokenRetrieved === "undefined") {
        return res.status(401).json({ msg: "Unauthorized" });
      }
      req.body = { ...req.body, object };
      next();
    }
  );
}
