import express from "express";
import { User } from "../entity/User";
import { getRepository } from "typeorm";
import { Token } from "../entity/Token";
import bcryptjs from "bcryptjs";
import { generateAccessToken, generateRefreshToken } from "../help_functions";

const router = express.Router();

router.post("/api/user/login", async (req, res) => {
  const { email, password } = req.body;

  const user = await getRepository(User).findOne({ where: { email } });
  if (user == null) {
    return res.status(400).send("Email lub hasło niepoprawne");
  }
  const { first_name, last_name, phone, password: userPassword } = user;

  try {
    if (await bcryptjs.compare(password, userPassword)) {
      const access_token = generateAccessToken({
        uuid: user.uuid,
        first_name,
        last_name,
        email,
        phone,
      });
      const refresh_token = generateRefreshToken({
        uuid: user.uuid,
        first_name,
        last_name,
        email,
        phone,
      });
      Token.create({
        refresh_token,
        access_token,
      }).save();
      return res.status(201).json({ access_token, refresh_token });
    } else {
      return res.status(400).json({ msg: "Email lub hasło niepoprawne" });
    }
  } catch {
    return res.status(500).json({ msg: "Internal server error" });
  }
});

export { router as loginUserRouter };
