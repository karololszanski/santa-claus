import express from "express";
import { Letter } from "../entity/Letter";
import { getRepository } from "typeorm";

const router = express.Router();

router.get("/api/all_letters", async (req, res) => {
  const letters = await getRepository(Letter).find({
    relations: ["orphanage", "category"],
    where: { step: 2 },
  });

  return res.json(
    letters.map((letter) => {
      return {
        ...letter,
        last_name: letter?.last_name?.slice(0, 1),
        orphanage: {
          name: letter.orphanage.name,
          email: letter.orphanage.email,
          phone: letter.orphanage.phone,
          voivodeship: letter.orphanage.voivodeship,
          postal_code: letter.orphanage.postal_code,
          city: letter.orphanage.city,
          street: letter.orphanage.street,
          building_number: letter.orphanage.building_number,
          lat: letter.orphanage.lat,
          lng: letter.orphanage.lng,
        },
      };
    })
  );
});

export { router as getAllLettersRouter };
