import express from "express";
import { User } from "../entity/User";
import { validate } from "class-validator";
import { getRepository } from "typeorm";
import { Token } from "../entity/Token";
import bcryptjs from "bcryptjs";
import { generateAccessToken, generateRefreshToken } from "../help_functions";

const router = express.Router();

router.post("/api/user", async (req, res) => {
  const { first_name, last_name, email, password, phone } = req.body;

  const userWithSamePhoneOrEmail = await getRepository(User)
    .createQueryBuilder("user")
    .where("email = :email OR phone = :phone", { email: email, phone: phone })
    .getOne();

  if (userWithSamePhoneOrEmail) {
    console.log("User with this email and/or phone exists");
    return res.status(400).json({
      msg: "User with this email and/or phone exists",
    });
  }

  try {
    const hashedPassword = await bcryptjs.hash(password, 10);
    const user = User.create({
      first_name,
      last_name,
      email,
      password: hashedPassword,
      phone,
    });

    const errors = await validate(user);

    if (errors?.length > 0) {
      console.log("Error: ", errors);
      return res.status(400).json(errors);
    } else {
      console.log("Zapisuje user'a");
      const access_token = generateAccessToken({
        uuid: user.uuid,
        first_name,
        last_name,
        email,
        phone,
      });
      const refresh_token = generateRefreshToken({
        uuid: user.uuid,
        first_name,
        last_name,
        email,
        phone,
      });
      Token.create({
        refresh_token,
        access_token,
      }).save();
      await user.save();
      return res.status(201).json({ access_token, refresh_token });
    }
  } catch (error) {
    console.log("Error tworzenie usera: ", { error });
    return res.status(500).json({ msg: "Internal server error" });
  }
});

export { router as createUserRouter };
