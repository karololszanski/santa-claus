import express from "express";
import { getRepository } from "typeorm";
import bcryptjs from "bcryptjs";
import jwt from "jsonwebtoken";
import { Token } from "../entity/Token";
import { Orphanage } from "../entity/Orphanage";
import { generateAccessToken, generateRefreshToken } from "../help_functions";
const router = express.Router();

router.post("/api/orphanage/login", async (req, res) => {
  const { email, password } = req.body;

  const orphanage = await getRepository(Orphanage).findOne({
    where: { email },
  });
  if (orphanage == null) {
    return res.status(400).send("Cannot find orphanage");
  }
  const { uuid, name, code, password: orphanagePassword } = orphanage;

  try {
    if (await bcryptjs.compare(password, orphanagePassword)) {
      const access_token = generateAccessToken({ uuid, name, email, code });
      const refresh_token = generateRefreshToken({ uuid, name, email, code });
      Token.create({
        refresh_token,
        access_token,
      }).save();
      return res.status(201).json({ access_token, refresh_token });
    } else {
      return res.status(400).json({ msg: "Not allowed" });
    }
  } catch {
    return res.status(500).json({ msg: "Internal server error" });
  }
});

export { router as loginOrphanageRouter };
