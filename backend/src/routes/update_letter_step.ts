import express from "express";
import { validate } from "class-validator";
import { getConnection, getRepository } from "typeorm";
import isUUID from "validator/lib/isUUID";
import { Letter } from "../entity/Letter";
import { authenticateToken } from "../help_functions";

const router = express.Router();

router.put("/api/letter/step", authenticateToken, async (req, res) => {
  const { letter_id, step, object } = req.body;
  let stepNumber = 0;
  try {
    stepNumber = parseInt(step, 10);
    if (Number.isNaN(stepNumber)) {
      return res.status(400).json({ msg: "Invalid step." });
    }
  } catch (error) {
    return res.status(400).json({ msg: "Invalid step." });
  }
  if (
    !letter_id ||
    typeof letter_id !== "string" ||
    (letter_id && !isUUID(letter_id))
  ) {
    console.log("Invalid letter_id.");
    return res.status(400).json({ msg: "Invalid letter_id." });
  } else if (typeof stepNumber !== "number") {
    console.log("Invalid step.");
    return res.status(400).json({ msg: "Invalid step." });
  }

  const letter = await getRepository(Letter).findOne({
    where: { uuid: letter_id },
    relations: ["user", "orphanage"],
  });

  if (letter) {
    if (
      letter.orphanage.uuid === object.uuid ||
      letter?.user?.uuid === object.uuid ||
      !letter.user
    ) {
      letter.step = stepNumber;
      const errors = await validate(letter);
      if (errors?.length > 0) {
        console.log("Error: ", errors);
        return res.status(400).json(errors);
      } else {
        await letter.save();
        if (step < 3 && letter.user) {
          await getConnection()
            .createQueryBuilder()
            .relation(Letter, "user")
            .of(letter.uuid)
            .set(null);
        }
        return res.status(200).json({
          msg: `letter step updated now: ${letter.step}`,
        });
      }
    } else {
      return res.status(403).json({ msg: "Forbidden" });
    }
  } else {
    return res.status(404).json({
      msg: "letter not found",
    });
  }
});

export { router as updateLetterStepRouter };
