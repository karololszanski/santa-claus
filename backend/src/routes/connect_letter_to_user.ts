import express from "express";
import { User } from "../entity/User";
import { getRepository } from "typeorm";
import isUUID from "validator/lib/isUUID";
import { Letter } from "../entity/Letter";
import { authenticateToken } from "../help_functions";

const router = express.Router();

router.put("/api/letter/connect", authenticateToken, async (req, res) => {
  const { letter_id, object } = req.body;
  const user_id = object.uuid;
  if (
    (user_id && typeof user_id !== "string") ||
    (user_id && !isUUID(user_id))
  ) {
    console.log("Invalid user_id.");
    return res.json({ msg: "Invalid user_id." });
  } else if (
    (letter_id && typeof letter_id !== "string") ||
    (letter_id && !isUUID(letter_id))
  ) {
    console.log("Invalid letter_id.");
    return res.json({ msg: "Invalid letter_id." });
  }

  const user = await getRepository(User).findOne(user_id);
  const letter = await getRepository(Letter).findOne({
    where: { uuid: letter_id },
    relations: ["user"],
  });

  console.log("Letter if: ", letter?.user);
  if (letter?.user) {
    return res.json({
      msg: "letter already taken",
    });
  }

  console.log("User: ", user, " Letter: ", letter);

  if (user && letter) {
    letter.user = user;
    letter.step = 3;

    await letter.save();

    return res.json({
      msg: "letter connected to user",
    });
  } else {
    return res.json({
      msg: "user or letter not found",
    });
  }
});

export { router as connectLetterToUserRouter };
