import express from "express";
import { Letter } from "../entity/Letter";
import { getRepository } from "typeorm";
import isUUID from "validator/lib/isUUID";
import { authenticateToken } from "../help_functions";
import { Orphanage } from "../entity/Orphanage";

const router = express.Router();

router.get("/api/letters/:step", authenticateToken, async (req, res) => {
  const { object } = req.body;
  const { step } = req.params;
  let stepNumber = 0;
  try {
    stepNumber = parseInt(step, 10);
    if (Number.isNaN(stepNumber)) {
      return res.status(400).json({ msg: "Invalid step." });
    }
  } catch (error) {
    return res.status(400).json({ msg: "Invalid step." });
  }
  const uuid = object.uuid;
  if ((uuid && typeof uuid !== "string") || (uuid && !isUUID(uuid))) {
    console.log("Invalid token");
    return res.json({ msg: "Invalid token" });
  } else if (typeof stepNumber !== "number") {
    console.log("Invalid step.");
    return res.status(400).json({ msg: "Invalid step." });
  }

  const orphanage = await getRepository(Orphanage).findOne(uuid);

  if (orphanage && typeof stepNumber === "number") {
    const letters = await getRepository(Letter).find({
      relations: ["user", "orphanage", "category"],
      where: { orphanage: orphanage.uuid, step: stepNumber },
    });

    return res.json(
      letters.map((letter) => {
        return {
          ...letter,
          orphanage: null,
          user: letter?.user
            ? {
                first_name: letter.user.first_name,
                last_name: letter.user.last_name,
                email: letter.user.email,
                phone: letter.user.phone,
              }
            : null,
        };
      })
    );
  } else {
    return res.status(400).json({ msg: "Bad request." });
  }
});

export { router as getLettersRouter };
