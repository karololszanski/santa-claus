import express from "express";
import { generateAccessToken, generateRefreshToken } from "../help_functions";
import { getConnection, getRepository } from "typeorm";
import { Token } from "../entity/Token";
import jwt from "jsonwebtoken";

const router = express.Router();

router.put("/api/token", async (req, res) => {
  const { refresh_token } = req.body;
  console.log("Refresh token: ", refresh_token);

  if (refresh_token === null) {
    return res.status(401).json({ msg: "Unauthorized" });
  }

  const token = await getRepository(Token).findOne(refresh_token);

  if (!token || typeof token === "undefined") {
    return res.status(403).json({ msg: "Forbidden" });
  }

  try {
    jwt.verify(
      refresh_token,
      process.env.REFRESH_TOKEN_SECRET as string,
      async (err: any, object: any) => {
        if (err) {
          return res.status(403).json({ msg: "Forbidden" });
        }

        console.log("Object: ", object);
        const { iat, ...rest } = object;
        const newAccessToken = generateAccessToken({ ...rest });
        const newRefreshToken = generateRefreshToken({ ...rest });

        await getConnection()
          .createQueryBuilder()
          .delete()
          .from(Token)
          .where("refresh_token = :refresh_token", { refresh_token })
          .execute();

        try {
          Token.create({
            refresh_token: newRefreshToken,
            access_token: newAccessToken,
          }).save();
        } catch (error) {
          return res.status(500).json({ msg: "Server error" });
        }
        return res.status(200).json({
          access_token: newAccessToken,
          refresh_token: newRefreshToken,
        });
      }
    );
  } catch {
    return res.status(500).json({ msg: "Internal Server Error" });
  }
  return;
});

export { router as newTokenRouter };
