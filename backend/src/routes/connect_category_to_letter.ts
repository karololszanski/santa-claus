import express from "express";
import { getRepository } from "typeorm";
import isUUID from "validator/lib/isUUID";
import { Letter } from "../entity/Letter";
import { Category } from "../entity/Category";

const router = express.Router();

router.put("/api/category/connect", async (req, res) => {
  const { category_id, letter_id } = req.body;

  if (
    (category_id && typeof category_id !== "string") ||
    (category_id && !isUUID(category_id))
  ) {
    console.log("Invalid category_id.");
    return res.status(400).json({ msg: "Invalid category_id." });
  } else if (
    (letter_id && typeof letter_id !== "string") ||
    (letter_id && !isUUID(letter_id))
  ) {
    console.log("Invalid letter_id.");
    return res.status(400).json({ msg: "Invalid letter_id." });
  }

  const category = await getRepository(Category).findOne(category_id);
  const letter = await getRepository(Letter).findOne({
    where: { uuid: letter_id },
    relations: ["category"],
  });

  console.log("category: ", category, " Letter: ", letter);

  if (category && letter) {
    letter.category = category;

    await letter.save();

    return res.status(200).json({
      msg: "letter connected to category",
    });
  } else {
    return res.status(400).json({
      msg: "category or letter not found",
    });
  }
});

export { router as connectCategoryToLetterRouter };
