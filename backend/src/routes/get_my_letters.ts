import express from "express";
import { Letter } from "../entity/Letter";
import { getRepository, MoreThanOrEqual } from "typeorm";
import isUUID from "validator/lib/isUUID";
import { authenticateToken } from "../help_functions";
import { User } from "../entity/User";

const router = express.Router();

router.get("/api/myletters", authenticateToken, async (req, res) => {
  const { object } = req.body;
  console.log("Req object: ", object);

  const uuid = object.uuid;
  if ((uuid && typeof uuid !== "string") || (uuid && !isUUID(uuid))) {
    console.log("Invalid token");
    return res.json({ msg: "Invalid token" });
  }

  const user = await getRepository(User).findOne(uuid);

  if (user) {
    const letters = await getRepository(Letter).find({
      relations: ["user", "orphanage", "category"],
      where: { user: user.uuid, step: MoreThanOrEqual(3) },
    });

    return res.json(
      letters.map((letter) => {
        return {
          ...letter,
          orphanage: {
            name: letter.orphanage.name,
            email: letter.orphanage.email,
            phone: letter.orphanage.phone,
            voivodeship: letter.orphanage.voivodeship,
            postal_code: letter.orphanage.postal_code,
            city: letter.orphanage.city,
            street: letter.orphanage.street,
            building_number: letter.orphanage.building_number,
            lat: letter.orphanage.lat,
            lng: letter.orphanage.lng,
          },
          user: null,
        };
      })
    );
  } else {
    return res.status(400).json({ msg: "Bad request." });
  }
});

export { router as getMyLettersRouter };
