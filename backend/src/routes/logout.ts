import express from "express";
import { getConnection, getRepository } from "typeorm";
import { Token } from "../entity/Token";

const router = express.Router();

router.delete("/api/logout", async (req, res) => {
  const authHeader = req.headers["authorization"];
  const access_token = authHeader && authHeader.split(" ")[1];

  if (access_token === null)
    return res.status(400).json({ msg: "Bad request" });

  const token = await getRepository(Token).findOne({
    where: { access_token: access_token },
  });
  if (!token || typeof token === "undefined") {
    return res.status(401).json({ msg: "Unauthorized" });
  }

  await getConnection()
    .createQueryBuilder()
    .delete()
    .from(Token)
    .where("access_token = :access_token", { access_token })
    .execute();

  return res.status(204).json({ msg: "Logout success" });
});

export { router as logoutRouter };
