import express from "express";
import { getRepository } from "typeorm";
import { Orphanage } from "../entity/Orphanage";

const router = express.Router();

router.get("/api/code/:code", async (req, res) => {
  const { code } = req.params;
  if (!code || code?.length !== 6 || typeof code !== "string") {
    return res.status(404).json({ msg: "Niepoprawny kod" });
  }
  const orphanage = await getRepository(Orphanage).findOne({ where: { code } });

  if (!orphanage || typeof orphanage === "undefined") {
    return res.status(404).json({ msg: "Niepoprawny kod" });
  } else {
    return res.status(200).json({ msg: "Poprawny kod" });
  }
});

export { router as checkOrphanageRouter };
