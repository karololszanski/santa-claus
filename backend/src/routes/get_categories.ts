import express from "express";
import { Category } from "../entity/Category";
import { getRepository } from "typeorm";

const router = express.Router();

router.get("/api/categories", async (req, res) => {
  const categories = await getRepository(Category).find();

  return res.json(categories);
});

export { router as getCategoriesRouter };
