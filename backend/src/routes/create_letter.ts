import express from "express";
import { Letter } from "../entity/Letter";
import { User } from "../entity/User";
import { validate } from "class-validator";
import { getRepository } from "typeorm";
import { Orphanage } from "../entity/Orphanage";
import isUUID from "validator/lib/isUUID";
import { Category } from "../entity/Category";

const router = express.Router();

router.post("/api/letter", async (req, res) => {
  const { first_name, last_name, text, user_id, orphanage_code, category_id } =
    req.body;

  if (
    (user_id && typeof user_id !== "string") ||
    (user_id && !isUUID(user_id))
  ) {
    console.log("Invalid user_id.");
    return res.json({ msg: "Invalid user_id." });
  } else if (orphanage_code && typeof orphanage_code !== "string") {
    console.log("Invalid orphanage_code. Must be string");
    return res.json({ msg: "Invalid orphanage_code. Must be string" });
  } else if (
    (category_id && typeof category_id !== "string") ||
    (category_id && !isUUID(category_id))
  ) {
    console.log("Invalid category_id.");
    return res.json({ msg: "Invalid category_id." });
  }
  const user = await getRepository(User)
    .createQueryBuilder("user")
    .where("uuid = :user_id", { user_id })
    .getOne();
  console.log("User: ", user);
  const orphanage = await getRepository(Orphanage)
    .createQueryBuilder("orphanage")
    .where("code = :orphanage_code", { orphanage_code })
    .getOne();
  const category = await getRepository(Category)
    .createQueryBuilder("category")
    .where("uuid = :category_id", { category_id })
    .getOne();
  console.log("category: ", category);

  if (!orphanage) {
    console.log("Not found orphanage");
    return res.json({ msg: "Not found orphanage" });
  } else if (user_id && !user) {
    console.log("Not found user");
    return res.json({ msg: "Not found user" });
  } else if (category_id && !category) {
    console.log("Not found category");
    return res.json({ msg: "Not found category" });
  }

  const letter = Letter.create({
    first_name,
    last_name,
    text,
    step: 1,
    user,
    orphanage,
    category,
  });

  const errors = await validate(letter);

  if (errors?.length > 0) {
    return res.status(400).json(errors);
  } else {
    await letter.save();
    return res.status(200).json(letter);
  }
});

export { router as createLetterRouter };
