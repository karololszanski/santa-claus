import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderToggler,
  CImage,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilMenu } from '@coreui/icons'
import Chip from '@mui/material/Chip'
import LogoutIcon from '@mui/icons-material/Logout'

import { logo } from 'src/assets/brand/logo'
import { signOut } from 'src/utils/token'
import { useHistory } from 'react-router-dom'
import { Typography } from '@mui/material'

const AppHeader = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const sidebarShow = useSelector((state) => state.sidebarShow)

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        <CHeaderToggler
          className="ps-1"
          onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" to="/">
          {/* <CIcon icon={logo} height={48} alt="Logo" /> */}
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: 'pointer', color: '#d32f2f' }}
          >
            Zostań Mikołajem
          </Typography>
        </CHeaderBrand>
        <CHeaderNav className="ms-3">
          <Chip
            icon={<LogoutIcon />}
            label="Wyloguj się"
            onClick={() => {
              signOut()
              history.push('/login')
            }}
          />
        </CHeaderNav>
      </CContainer>
    </CHeader>
  )
}

export default AppHeader
