import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="https://zostanmikolajem.org" target="_blank" rel="noopener noreferrer">
          Zostań Mikołajem
        </a>
        <span className="ms-1">&copy; 2021</span>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
