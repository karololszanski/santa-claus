import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))

//Letters
const LettersRejected = React.lazy(() => import('./views/letters/LettersRejected/LettersRejected'))
const LettersForApproval = React.lazy(() =>
  import('./views/letters/LettersForApproval/LettersForApproval'),
)
const LettersApproved = React.lazy(() => import('./views/letters/LettersApproved/LettersApproved'))
const LettersInProgress = React.lazy(() =>
  import('./views/letters/LettersInProgress/LettersInProgress'),
)

//Packages
const PackagesDelivered = React.lazy(() =>
  import('./views/packages/PackagesDelivered/PackagesDelivered'),
)
const PackagesSended = React.lazy(() => import('./views/packages/PackagesSended/PackagesSended'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/main', name: 'Główna', component: Dashboard },
  { path: '/letters', name: 'Listy', component: LettersForApproval, exact: true },
  { path: '/letters/rejected', name: 'Odrzucone', component: LettersRejected },
  { path: '/letters/for-approval', name: 'Do akceptacji', component: LettersForApproval },
  { path: '/letters/approved', name: 'Zaakceptowane', component: LettersApproved },
  { path: '/letters/in-progress', name: 'W trakcie realizacji', component: LettersInProgress },
  { path: '/packages', name: 'Paczki', component: PackagesSended, exact: true },
  { path: '/packages/sended', name: 'Wysłane', component: PackagesSended },
  { path: '/packages/delivered', name: 'Dostarczone', component: PackagesDelivered },
]

export default routes
