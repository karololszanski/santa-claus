import React from 'react'
import TableLetters from '../tableLetters'

const LettersInProgress = () => {
  return (
    <>
      <TableLetters inProgress />
    </>
  )
}

export default LettersInProgress
