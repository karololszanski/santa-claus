import React from 'react'
import TableLetters from '../tableLetters'

const LettersForApproval = () => {
  return (
    <>
      <TableLetters forApproval />
    </>
  )
}

export default LettersForApproval
