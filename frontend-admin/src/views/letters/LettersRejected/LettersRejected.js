import React from 'react'
import TableLetters from '../tableLetters'

const LettersRejected = () => {
  return (
    <>
      <TableLetters rejected />
    </>
  )
}

export default LettersRejected
