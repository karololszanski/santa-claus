export const prepareItems = (letters) => {
  const mappedItems = letters
    .filter(
      (item, index, self) => index === self.findIndex((element) => item.name === element.name),
    )
    .sort((a, b) => {
      if (a.name < b.name) {
        return -1
      }
      if (a.name > b.name) {
        return 1
      }
      return 0
    })
    .sort((a, b) => {
      if (a.parent < b.parent) {
        return -1
      }
      if (a.parent > b.parent) {
        return 1
      }
      return 0
    })

  return mappedItems
}
