import React from 'react'
import TableLetters from '../tableLetters'

const LettersApproved = () => {
  return (
    <>
      <TableLetters approved />
    </>
  )
}

export default LettersApproved
