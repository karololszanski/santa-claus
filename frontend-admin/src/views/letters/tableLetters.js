import React from 'react'
import PropTypes from 'prop-types'
import {
  Autocomplete,
  Box,
  Button,
  Collapse,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableSortLabel,
  Toolbar,
  TableRow,
  TextField,
  Typography,
} from '@mui/material'
import { visuallyHidden } from '@mui/utils'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import useLetters from '../../api/useLetters'
import useCategories from '../../api/useCategories'
import { prepareItems } from './lettersHelpFunctions'
import { connectCategory } from 'src/api/letter/connectCategoryWithLetter'
import { updateLetterStep } from 'src/api/letter/updateLetterStep'
import { toast } from 'react-toastify'
import { checkToken } from 'src/utils/token'
import NoLetterFound from './tableHelpComponents/noLetterFound'
import LoadingTable from './tableHelpComponents/loadingTable'

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)
}

const headCells = [
  {
    id: 'last_name',
    label: 'Nazwisko',
  },
  {
    id: 'first_name',
    label: 'Imię',
  },
  {
    id: 'created_at',
    label: 'Data utworzenia',
  },
  {
    id: 'category?.name',
    label: 'Kategoria',
  },
]

function EnhancedTableHead(props) {
  const { order, orderBy, rowCount, onRequestSort } = props
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <TableHead>
      <TableRow>
        <TableCell />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'right'}
            padding={'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
}

const EnhancedTableToolbar = (props) => {
  const { rejected, forApproval, approved, inProgress, delivered, sended } = props

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
      }}
    >
      <Typography sx={{ flex: '1 1 100%' }} variant="h6" id="tableTitle" component="div">
        {rejected && 'Listy odrzucone'}
        {forApproval && 'Listy do akceptacji'}
        {approved && 'Listy zaakceptowane'}
        {inProgress && 'Listy w trakcie realizacji'}
        {sended && 'Paczki wysłane'}
        {delivered && 'Paczki dostarczone'}
      </Typography>
    </Toolbar>
  )
}

const TableLetters = (props) => {
  const { rejected, forApproval, approved, inProgress, delivered, sended } = props
  const [order, setOrder] = React.useState('asc')
  const [orderBy, setOrderBy] = React.useState('d')
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(5)
  const { data: dataLetters, error: errorLetters } = useLetters(
    rejected ? 0 : forApproval ? 1 : approved ? 2 : inProgress ? 3 : sended ? 4 : 5,
  )
  const { data: dataCategories, error: errorCategories } = useCategories()
  const [letters, setLetters] = React.useState(null)
  const [categories, setCategories] = React.useState(null)

  React.useEffect(() => {
    if (dataLetters) {
      setLetters(
        dataLetters?.map((item) => {
          console.log('Ustawiam item: ', {
            ...item,
            open: false,
            category_api: item?.category?.name,
          })
          return { ...item, open: false, category_api: item?.category?.name }
        }),
      )
    }
  }, [dataLetters])

  React.useEffect(() => {
    if (dataCategories) {
      setCategories(prepareItems(dataCategories))
    }
  }, [dataCategories])

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - letters.length) : 0

  function Row(props) {
    const { row, index, categories } = props
    console.log('Categories and row: ', categories, row)

    const onItemChange = (e, prop, value) => {
      console.log('Event: ', e, ' Value: ', value)
      setLetters(
        letters.map((item) => {
          if (item.uuid === row.uuid) {
            console.log('Item znaleziony: ', item.uuid, row.uuid, { ...item, [prop]: value })
            return { ...item, [prop]: value }
          } else {
            return { ...item }
          }
        }),
      )
    }

    const changeCategory = (e) => {
      connectCategory(
        row?.uuid,
        row?.category?.uuid,
        (response) => {
          onItemChange(e, 'category_api', row?.category?.name)
        },
        (error) => {
          console.log('Error: ', error)
          toast.error('Wystąpił błąd', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: 'error',
          })
        },
      )
    }

    const updateLetterStepAction = (letter_id, step, category) => {
      if (step < 2 || category) {
        updateLetterStep(
          letter_id,
          step,
          () => {
            setLetters(
              letters.filter((letter) => {
                if (letter?.uuid === letter_id) {
                  return false
                } else {
                  return true
                }
              }),
            )
          },
          (error) => {
            if (error?.response?.status === 401) {
              setTimeout(() => {
                if (error?.response?.status === 401 && checkToken()) {
                  setLetters(
                    letters.filter((letter) => {
                      if (letter?.uuid === letter_id) {
                        return false
                      } else {
                        return true
                      }
                    }),
                  )
                }
              }, 1000)
            } else {
              console.log('Error updatowanie kroku: ', { error })
            }
          },
        )
      } else {
        toast.info('Musisz wybrać kategorię', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: 'error',
        })
      }
    }

    return (
      <>
        <TableRow
          hover
          tabIndex={-1}
          key={index}
          onClick={(e) => onItemChange(e, 'open', !row.open)}
        >
          <TableCell>
            <IconButton aria-label="expand row" size="small">
              {row?.open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell align="right">{row?.last_name}</TableCell>
          <TableCell align="right">{row?.first_name}</TableCell>
          <TableCell align="right">
            {new Date(row?.created_at).toLocaleString('pl-PL', { timeZone: 'CET' })}
          </TableCell>
          <TableCell align="right">{row?.category_api ?? 'Nieprzypisano'}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={row?.open} timeout="auto" unmountOnExit>
              <Box
                sx={{
                  alignItems: 'center',
                  display: 'flex',
                  flexDirection: 'column',
                  m: 1,
                }}
              >
                <Grid container m={1}>
                  {forApproval && (
                    <>
                      <Grid item xs={8}>
                        <Autocomplete
                          disabled={forApproval ? false : true}
                          options={categories}
                          groupBy={(item) => item.parent}
                          getOptionLabel={(item) => item.name ?? ''}
                          onChange={(e, value) => onItemChange(e, 'category', value)}
                          isOptionEqualToValue={(option, value) => {
                            return JSON.stringify(option) === JSON.stringify(value)
                          }}
                          value={
                            categories.find((item) => item.name === row?.category?.name)?.name
                              ? categories.find((item) => item.name === row?.category?.name)
                              : null
                          }
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant="standard"
                              label={'Kategoria'}
                              placeholder={'Wybierz kategorie'}
                            />
                          )}
                        />
                      </Grid>
                      <Grid
                        item
                        xs={4}
                        sx={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Button
                          disabled={row?.category?.name === row?.category_api ? true : false}
                          variant="contained"
                          color="success"
                          sx={{ m: 2 }}
                          onClick={changeCategory}
                        >
                          Zmień kategorie
                        </Button>
                      </Grid>
                    </>
                  )}
                  {(inProgress || sended || delivered) && (
                    <Grid
                      item
                      xs={12}
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '30px 20px',
                      }}
                    >
                      <TextField
                        label="Imię (osoba realizująca list)"
                        variant="standard"
                        disabled
                        value={row?.user?.first_name}
                        sx={{ margin: '5px', width: '300px' }}
                      />
                      <TextField
                        label="Nazwisko (osoba realizująca list)"
                        variant="standard"
                        disabled
                        value={row?.user?.last_name}
                        sx={{ margin: '5px', width: '300px' }}
                      />
                      <TextField
                        label="Email (osoba realizująca list)"
                        variant="standard"
                        disabled
                        value={row?.user?.email}
                        sx={{ margin: '5px', width: '300px' }}
                      />
                      <TextField
                        label="Telefon (osoba realizująca list)"
                        variant="standard"
                        disabled
                        value={row?.user?.phone}
                        sx={{ margin: '5px', width: '300px' }}
                      />
                    </Grid>
                  )}
                </Grid>
                <Paper elevation={6} sx={{ m: 1 }}>
                  <Typography
                    component="div"
                    sx={{
                      p: 2,
                      whiteSpace: 'pre-line',
                      fontSize: '18px',
                      maxHeight: '500px',
                      overflow: 'auto',
                    }}
                  >
                    {row?.text}
                  </Typography>
                </Paper>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: '10px',
                  }}
                >
                  {!rejected && !inProgress && !sended && (
                    <Box>
                      <Button
                        variant="contained"
                        color="error"
                        sx={{ m: 2 }}
                        onClick={() =>
                          updateLetterStepAction(row?.uuid, row?.step - 1, row?.category_api)
                        }
                      >
                        {forApproval && 'Odrzuć list'}
                        {approved && 'Przenieś do: Do akceptacji'}
                        {delivered && 'Przenieś do: Wysłane'}
                      </Button>
                    </Box>
                  )}
                  {!approved && !inProgress && !delivered && (
                    <Box>
                      <Button
                        variant="contained"
                        color="success"
                        sx={{ m: 2 }}
                        onClick={() =>
                          updateLetterStepAction(row?.uuid, row?.step + 1, row?.category_api)
                        }
                      >
                        {rejected && 'Przenieś do: Do akceptacji'}
                        {forApproval && 'Zaakceptuj list'}
                        {sended && 'Potwierdź otrzymanie przesyłki'}
                      </Button>
                    </Box>
                  )}
                </Box>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </>
    )
  }

  Row.propTypes = {
    row: PropTypes.shape({
      uuid: PropTypes.string.isRequired,
      last_name: PropTypes.string.isRequired,
      first_name: PropTypes.string.isRequired,
      created_at: PropTypes.string.isRequired,
      category: PropTypes.shape({
        name: PropTypes.string.isRequired,
        parent: PropTypes.string.isRequired,
        uuid: PropTypes.string.isRequired,
      }),
      category_api: PropTypes.string,
      text: PropTypes.string.isRequired,
      step: PropTypes.number.isRequired,
      user: PropTypes.shape({
        last_name: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        email: PropTypes.string.isRequired,
        phone: PropTypes.string.isRequired,
      }),
      open: PropTypes.bool.isRequired,
    }).isRequired,
    index: PropTypes.number.isRequired,
    categories: PropTypes.arrayOf(PropTypes.string.isRequired),
  }

  return (
    <>
      {letters && categories && (
        <Box sx={{ width: '100%' }}>
          <Paper>
            <EnhancedTableToolbar
              rejected={rejected}
              forApproval={forApproval}
              approved={approved}
              inProgress={inProgress}
              sended={sended}
              delivered={delivered}
            />
            <TableContainer>
              <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle" size={'medium'}>
                {letters ? (
                  letters?.length > 0 ? (
                    <>
                      <EnhancedTableHead
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        rowCount={letters.length}
                      />
                      <TableBody>
                        <>
                          {letters
                            .slice()
                            .sort(getComparator(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => (
                              <Row key={index} row={row} index={index} categories={categories} />
                            ))}

                          {emptyRows > 0 && (
                            <TableRow
                              style={{
                                height: 53 * emptyRows,
                              }}
                            >
                              <TableCell colSpan={6} />
                            </TableRow>
                          )}
                        </>
                      </TableBody>
                    </>
                  ) : (
                    <NoLetterFound />
                  )
                ) : (
                  <LoadingTable />
                )}
              </Table>
            </TableContainer>
            <TablePagination
              sx={{ 'p, a': { m: 0 } }}
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={letters?.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              labelRowsPerPage={'Listów na stronie'}
              labelDisplayedRows={function defaultLabelDisplayedRows({ from, to, count }) {
                return `${from}-${to} z ${count !== -1 ? count : `więcej niż ${to}`}`
              }}
            />
          </Paper>
        </Box>
      )}
    </>
  )
}

TableLetters.propTypes = {
  rejected: PropTypes.bool,
  forApproval: PropTypes.bool,
  approved: PropTypes.bool,
  inProgress: PropTypes.bool,
  delivered: PropTypes.bool,
  sended: PropTypes.bool,
}

EnhancedTableToolbar.propTypes = TableLetters.propTypes

export default TableLetters
