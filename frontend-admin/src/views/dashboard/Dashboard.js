import React, { useState, useEffect } from 'react'
import { Grid, Stepper, Step, StepLabel, StepContent, Box, Button, Typography } from '@mui/material'
import { decodeToken } from 'src/utils/token'
import { CLIENT_PAGE } from 'src/constants/projectConstants'

const Dashboard = () => {
  const isBrowser = typeof window !== 'undefined'
  const [decodedData, setDecodedData] = useState(null)
  useEffect(() => {
    setDecodedData(decodeToken())
  }, [isBrowser && localStorage.getItem('token')])
  const [activeStep, setActiveStep] = React.useState(0)
  const steps1 = [
    {
      label: 'Udostępnij link do przeglądarki',
      description: `${CLIENT_PAGE}/letter/?code=${decodedData?.code}`,
    },
  ]
  const steps2 = [
    {
      label: 'Udostępnij link do przeglądarki',
      description: `${CLIENT_PAGE}/letter`,
    },
    {
      label: 'Podaj kod ośrodka',
      description: decodedData?.code,
    },
  ]
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  return (
    <>
      <Typography variant="h2" sx={{ textAlign: 'center', mt: 4 }}>
        Witaj!
      </Typography>
      <Typography variant="h2" sx={{ fontWeight: 'bold', textAlign: 'center' }}>
        {decodedData?.name}
      </Typography>
      <Typography variant="h5" sx={{ fontWeight: 'bold', textAlign: 'center' }}>
        {`(${decodedData?.code})`}
      </Typography>
      <Typography variant="h5" sx={{ mt: 8 }}>
        Instrukcja pisania listów przez dzieci:
      </Typography>
      <Grid container spacing={2} m={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
            Najprostszy sposób
          </Typography>
          <Stepper activeStep={0} orientation="vertical">
            {steps1.map((step, index) => (
              <Step key={step.label}>
                <StepLabel>{step.label}</StepLabel>
                <StepContent>
                  <Typography sx={{ fontSize: '24px' }}>{step.description}</Typography>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
            Alternatywna metoda
          </Typography>
          <Stepper activeStep={activeStep} orientation="vertical">
            {steps2.map((step, index) => (
              <Step key={step.label}>
                <StepLabel>{step.label}</StepLabel>
                <StepContent>
                  <Typography sx={{ fontSize: '24px' }}>{step.description}</Typography>
                  <Box sx={{ mb: 2 }}>
                    <div>
                      {steps2.length > 1 && index !== steps2.length - 1 && (
                        <Button variant="contained" onClick={handleNext} sx={{ mt: 1, mr: 1 }}>
                          {'Następny krok'}
                        </Button>
                      )}
                      {index !== 0 && (
                        <Button
                          variant="contained"
                          disabled={index === 0}
                          onClick={handleBack}
                          sx={{ mt: 1, mr: 1 }}
                        >
                          {'Poprzedni krok'}
                        </Button>
                      )}
                    </div>
                  </Box>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </Grid>
      </Grid>
    </>
  )
}

export default Dashboard
