import React, { useState } from 'react'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { toast } from 'react-toastify'
import { login } from 'src/api/auth/login'
import { saveTokenCredentials } from 'src/utils/token'

const Login = () => {
  const history = useHistory()
  const [values, setValues] = useState({
    email: {
      value: '',
      error: '',
      init: false,
    },
    password: {
      value: '',
      error: '',
      showPassword: false,
      init: false,
    },
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    if (isFormValid()) {
      console.log(values.email, values.password)
      login(
        values.email.value,
        values.password.value,
        (response) => {
          console.log(response)
          saveTokenCredentials(response)
          axios.defaults.headers.common.authorization = `Bearer ${
            JSON.parse(localStorage.getItem('token')).access_token
          }`
          history.push('/main')
        },
        (error) => {
          toast.error(error?.response?.data?.msg, {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: error?.response?.data?.msg,
          })
        },
      )
    } else {
      toast.error('Sprawdź czy wszytskie pola są poprawnie wypełnione', {
        position: 'top-center',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: 'invalidForm',
      })
    }
  }

  const handleChange =
    (prop, error, init = false) =>
    (event) => {
      setValues({
        ...values,
        [prop]: {
          ...values[prop],
          value: event.target.value,
          error: error,
          init: init ? true : values[prop].init,
        },
      })
    }

  const isFormValid = () => {
    return !(
      !values.email.value ||
      checkEmail(values.email.value).length > 0 ||
      !values.password.value ||
      values.password.value.length < 8 ||
      values.password.value.length > 128
    )
  }

  const checkEmail = (email) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase()) ? '' : 'Nieprawidłowy adres email'
  }

  const checkTextLength = (prop, minLength, maxLength) => (event) => {
    if (event.target.value.length < minLength) {
      return `${prop} musi zawierać co najmniej ${minLength} ${
        minLength < 5 ? (minLength === 1 ? 'znak' : 'znaki') : 'znaków'
      }`
    } else if (event.target.value.length > maxLength) {
      return `${prop} musi zawierać najwyżej ${maxLength} znaki`
    }
    return ''
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Logowanie</h1>
                    <p className="text-medium-emphasis">Zaloguj się do swojego konta</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        placeholder="Email"
                        type="email"
                        value={values.email.value}
                        invalid={values.email.init && values.email.error.length > 0}
                        onBlur={(e) => {
                          handleChange('email', checkEmail(e.target.value), true)(e)
                        }}
                        onChange={(e) => {
                          handleChange('email', checkEmail(e.target.value))(e)
                        }}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Hasło"
                        value={values.password.value}
                        invalid={values.password.init && values.password.error.length > 0}
                        onChange={(e) => {
                          handleChange(
                            'password',
                            checkTextLength(e.target.placeholder, 8, 32)(e),
                          )(e)
                        }}
                        onBlur={(e) => {
                          handleChange(
                            'password',
                            checkTextLength(e.target.placeholder, 8, 32)(e),
                            true,
                          )(e)
                        }}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={12}>
                        <CButton color="primary" className="px-4" onClick={handleSubmit}>
                          Zaloguj się
                        </CButton>
                      </CCol>
                      {/* <CCol xs={6} className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol> */}
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5">
                <CCardBody className="text-center">
                  <div>
                    <h2>Problem z logowaniem?</h2>
                    <p>Skontaktuj się z naszym działem supportu.</p>
                    <a href="mailto:me@karololszanski.com">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>
                        Kontakt
                      </CButton>
                    </a>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
