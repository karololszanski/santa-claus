import React from 'react'
import TableLetters from '../../letters/tableLetters'

const PackagesDelivered = () => {
  return (
    <>
      <TableLetters delivered />
    </>
  )
}

export default PackagesDelivered
