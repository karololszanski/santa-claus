import useSWR from 'swr'
import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'
import { newToken } from 'src/api/auth/newToken'
import { toast } from 'react-toastify'
import { saveTokenCredentials, signOut } from 'src/utils/token'
import { useHistory } from 'react-router-dom'

const fetcher = async (url) =>
  await axios
    .get(url, {
      headers: {
        authorization: `Bearer ${JSON.parse(localStorage.getItem('token'))?.access_token}`,
      },
    })
    .then((res) => res.data)

const useLetters = (step) => {
  const history = useHistory()
  const { data, error } = useSWR(`${API_ENDPOINT}/letters/${step}`, fetcher, {
    revalidateOnFocus: false,
    onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
      if (
        JSON.parse(localStorage.getItem('token'))?.refresh_token &&
        error?.response?.status === 401 &&
        retryCount <= 2
      ) {
        newToken(
          JSON.parse(localStorage.getItem('token'))?.refresh_token,
          (response) => {
            console.log('Dostałem nowy token: ', response)
            saveTokenCredentials(response)
            axios.defaults.headers.common.authorization = `Bearer ${response.access_token}`
            setTimeout(() => revalidate({ retryCount }), 1000)
          },
          () => {},
        )
      } else if (
        !JSON.parse(localStorage.getItem('token'))?.refresh_token ||
        (error?.response?.status === 401 && retryCount > 2)
      ) {
        toast.error('Nastąpiło wylogowanie', {
          position: 'top-center',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: 'Unauthorized1',
        })
        signOut()
        history.push('/login')
      }

      setTimeout(() => revalidate({ retryCount }), 5000)
    },
  })

  return { data, error }
}

export default useLetters
