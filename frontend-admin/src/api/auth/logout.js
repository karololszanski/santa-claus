import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'

export function logout(onSuccess, onError) {
  axios
    .delete(`${API_ENDPOINT}/logout`)
    .then((response) => {
      console.log('Response: ', response)
      onSuccess(response.data)
    })
    .catch((error) => {
      onError(error)
    })
}
