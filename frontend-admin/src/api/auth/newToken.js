import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'

export function newToken(refresh_token, onSuccess, onError) {
  const uninterceptedAxiosInstance = axios.create()
  uninterceptedAxiosInstance
    .put(`${API_ENDPOINT}/token`, {
      refresh_token: refresh_token,
    })
    .then((response) => {
      console.log('Response: ', response)
      onSuccess(response.data)
    })
    .catch((error) => {
      onError(error.response)
    })
}
