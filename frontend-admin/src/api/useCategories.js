import useSWR from 'swr'
import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'

const fetcher = async (url) => await axios.get(url).then((res) => res.data)
const useCategories = () => {
  const { data, error } = useSWR(`${API_ENDPOINT}/categories`, fetcher, {
    revalidateOnFocus: false,
    dedupingInterval: 600000, // 10min
  })

  return { data, error }
}

export default useCategories
