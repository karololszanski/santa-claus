import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'

export function updateLetterStep(letter_id, step, onSuccess, onError) {
  axios
    .put(`${API_ENDPOINT}/letter/step`, {
      letter_id,
      step,
    })
    .then((response) => {
      onSuccess(response.data)
    })
    .catch((error) => {
      onError(error)
    })
}
