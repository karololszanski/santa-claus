import axios from 'axios'
import { API_ENDPOINT } from 'src/constants/projectConstants'

export function connectCategory(letter_id, category_id, onSuccess, onError) {
  axios
    .put(`${API_ENDPOINT}/category/connect`, {
      letter_id,
      category_id,
    })
    .then((response) => {
      onSuccess(response.data)
    })
    .catch((error) => {
      onError(error)
    })
}
