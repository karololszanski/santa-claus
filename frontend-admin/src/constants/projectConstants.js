export const PROJECT_TITLE = 'Zostań Mikołajem'
export const API_ENDPOINT =
  process.env.NODE_ENV === 'production'
    ? 'https://api-santa.herokuapp.com/api'
    : 'http://localhost:8080/api'
export const CLIENT_PAGE = 'https://client-santa.vercel.app'
