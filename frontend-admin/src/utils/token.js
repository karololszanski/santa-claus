import { logout } from 'src/api/auth/logout'
import axios from 'axios'
import jwt_decode from 'jwt-decode'

export const saveTokenCredentials = (response) => {
  localStorage.setItem(
    'token',
    JSON.stringify({
      access_token: response.access_token,
      refresh_token: response.refresh_token,
    }),
  )
}

export const signOut = () => {
  // const history = useHistory()
  logout(
    () => {
      delete axios.defaults.headers.common.authorization
      localStorage.removeItem('token')
      // history.push('/login')
    },
    () => {
      delete axios.defaults.headers.common.authorization
      localStorage.removeItem('token')
      // history.push('/login')
    },
  )
}

export const getUserData = () => {
  const isBrowser = typeof window !== 'undefined'
  if (
    isBrowser &&
    localStorage.getItem('token') &&
    JSON.parse(localStorage.getItem('token')).access_token &&
    JSON.parse(localStorage.getItem('token')).refresh_token
  ) {
    return jwt_decode(JSON.parse(localStorage.getItem('token')).access_token)
  } else {
    return null
  }
}

export const checkToken = () => {
  const isBrowser = typeof window !== 'undefined'
  if (
    isBrowser &&
    localStorage.getItem('token') &&
    JSON.parse(localStorage.getItem('token')).access_token &&
    JSON.parse(localStorage.getItem('token')).refresh_token
  ) {
    const decoded = jwt_decode(JSON.parse(localStorage.getItem('token')).access_token)
    if (decoded && decoded.exp > Date.now() / 1000) {
      return true
    }
  }
  return false
}

export const decodeToken = () => {
  const isBrowser = typeof window !== 'undefined'
  if (
    isBrowser &&
    localStorage.getItem('token') &&
    JSON.parse(localStorage.getItem('token')).access_token
  ) {
    return jwt_decode(JSON.parse(localStorage.getItem('token')).access_token)
  }
  return null
}
