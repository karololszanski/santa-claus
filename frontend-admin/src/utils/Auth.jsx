import React from 'react'
import { newToken } from 'src/api/auth/newToken'
import axios from 'axios'
import { useEffect } from 'react'
import { toast } from 'react-toastify'
import { saveTokenCredentials, signOut } from './token'
import { useHistory } from 'react-router-dom'
import { API_ENDPOINT } from 'src/constants/projectConstants'

const Auth = () => {
  const isBrowser = typeof window !== 'undefined'
  const history = useHistory()

  useEffect(() => {
    const myInterceptorRequest = axios.interceptors.request.use(
      (config) => {
        if (isBrowser && localStorage.getItem('token')) {
          const accessToken = JSON.parse(localStorage.getItem('token')).access_token
          return {
            ...config,
            headers: {
              ...config.headers,
              authorization: accessToken ? `Bearer ${accessToken}` : config.headers.authorization,
            },
          }
        } else {
          return config
        }
      },
      (error) => {
        return Promise.reject(error)
      },
    )
    const myInterceptorResponse = axios.interceptors.response.use(
      (response) => {
        return response
      },
      async (error) => {
        console.log('Response error: ', error, { error })
        const originalConfig = error.config
        if (
          error?.response?.status === 401 &&
          error?.config?.url.slice(0, -2) !== `${API_ENDPOINT}/letters` &&
          isBrowser &&
          localStorage.getItem('token')
        ) {
          if (!originalConfig._retry) {
            originalConfig._retry = true
            try {
              newToken(
                JSON.parse(localStorage.getItem('token')).refresh_token,
                (response) => {
                  saveTokenCredentials(response)
                  axios.defaults.headers.common.authorization = `Bearer ${response.access_token}`
                  return axios(originalConfig)
                },
                (error) => {
                  return Promise.reject(error)
                },
              )
            } catch (_error) {
              if (_error?.response?.data) {
                return Promise.reject(_error?.response?.data)
              }
              return Promise.reject(_error)
            }
          } else {
            toast.error('Nastąpiło wylogowanie', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              toastId: 'Unauthorized1',
            })
            signOut()
            history.push('/login')
          }
        } else if (
          error?.response?.status === 401 &&
          error?.config?.url.slice(0, -2) !== `${API_ENDPOINT}/letters`
        ) {
          toast.info('Musisz być zalogowany, aby wykonać tą akcję', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: 'Unauthorized2',
          })
        } else if (error?.response?.status === 403) {
          toast.info('Nie masz dostępu do tej akcji', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: 'Forbidden',
          })
        }
        return Promise.reject(error)
      },
    )

    if (
      isBrowser &&
      localStorage.getItem('token') &&
      JSON.parse(localStorage.getItem('token')).access_token
    ) {
      axios.defaults.headers.common.authorization = `Bearer ${
        JSON.parse(localStorage.getItem('token')).access_token
      }`
    }
    return () => {
      axios.interceptors.request.eject(myInterceptorRequest)
      axios.interceptors.response.eject(myInterceptorResponse)
      delete axios.defaults.headers.common.authorization
    }
  }, [isBrowser && localStorage.getItem('token')])

  return <></>
}

export default Auth
