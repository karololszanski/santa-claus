export const HOME_PAGE = "/";
export const PROFILE_PAGE = "/profile";
export const LETTERS_PAGE = "/myletters";
export const AUTH_PAGE = "/auth";
export const LOGIN_QUERY = "login";
export const REGISTER_QUERY = "register";
export const RESET_PASSWORD_QUERY = "reset";
export const TERMS_AND_SERVICES_PAGE = "/terms";
export const PRIVACY_POLICY_PAGE = "/privacy";
export const CONTACT_PAGE = "/contact";

export const HOME_MENU_ITEM = {
  defaultMessage: "Strona Główna",
  href: HOME_PAGE,
};
export const PROFILE_MENU_ITEM = {
  defaultMessage: "Profil",
  href: PROFILE_PAGE,
};
export const LETTERS_MENU_ITEM = {
  defaultMessage: "Moje listy",
  href: LETTERS_PAGE,
};
export const AUTH_MENU_ITEM = {
  defaultMessage: "Zaloguj się/Zarejestruj się",
  href: AUTH_PAGE,
};
export const LOGIN_MENU_ITEM = {
  defaultMessage: "Zaloguj się",
  href: AUTH_PAGE,
  query: { type: LOGIN_QUERY },
};
export const REGISTER_MENU_ITEM = {
  defaultMessage: "Zarejestruj się",
  href: AUTH_PAGE,
  query: { type: REGISTER_QUERY },
};
export const RESET_PASSWORD_MENU_ITEM = {
  defaultMessage: "Zarejestruj się",
  href: AUTH_PAGE,
  query: { type: RESET_PASSWORD_QUERY },
};
export const TERMS_AND_SERVICES_MENU_ITEM = {
  defaultMessage: "Regulamin",
  href: TERMS_AND_SERVICES_PAGE,
};
export const PRIVACY_POLICY_MENU_ITEM = {
  defaultMessage: "Polityka prywatności",
  href: PRIVACY_POLICY_PAGE,
};
export const CONTACT_MENU_ITEM = {
  defaultMessage: "Kontakt",
  href: CONTACT_PAGE,
};

export const AUTHORIZED_MENU_ITEMS = [
  HOME_MENU_ITEM,
  LETTERS_MENU_ITEM,
  // PROFILE_MENU_ITEM,
  // TERMS_AND_SERVICES_MENU_ITEM,
  // PRIVACY_POLICY_MENU_ITEM,
  // CONTACT_MENU_ITEM,
];

export const UNAUTHORIZED_MENU_ITEMS = [
  HOME_MENU_ITEM,
  AUTH_MENU_ITEM,
  // TERMS_AND_SERVICES_MENU_ITEM,
  // PRIVACY_POLICY_MENU_ITEM,
  // CONTACT_MENU_ITEM,
];
