import { logout } from "api/auth/logout";
import axios from "axios";
import { HOME_PAGE } from "constants/navigationConstants";
import jwt_decode from "jwt-decode";
import router from "next/dist/client/router";

export const saveTokenCredentials = (response) => {
  localStorage.setItem(
    "token",
    JSON.stringify({
      access_token: response.access_token,
      refresh_token: response.refresh_token,
    })
  );
};

export const signOut = () => {
  logout(
    () => {
      delete axios.defaults.headers.common.authorization;
      localStorage.removeItem("token");
      router.push(HOME_PAGE);
    },
    () => {
      delete axios.defaults.headers.common.authorization;
      localStorage.removeItem("token");
      router.push(HOME_PAGE);
    }
  );
};

export const getUserData = () => {
  const isBrowser = typeof window !== "undefined";
  if (
    isBrowser &&
    localStorage.getItem("token") &&
    JSON.parse(localStorage.getItem("token")).access_token &&
    JSON.parse(localStorage.getItem("token")).refresh_token
  ) {
    return jwt_decode(
      JSON.parse(localStorage.getItem("token")).access_token
    ) as any;
  } else {
    return null;
  }
};

export const checkToken = () => {
  const isBrowser = typeof window !== "undefined";
  if (
    isBrowser &&
    localStorage.getItem("token") &&
    JSON.parse(localStorage.getItem("token")).access_token &&
    JSON.parse(localStorage.getItem("token")).refresh_token
  ) {
    const decoded = jwt_decode(
      JSON.parse(localStorage.getItem("token")).access_token
    ) as any;
    if (decoded && decoded.exp > Date.now() / 1000) {
      return true;
    }
  }
  return false;
};
