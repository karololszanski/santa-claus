import { Inbox, Mail } from "@mui/icons-material";
import {
  Box,
  Button,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import React, { useEffect, useState } from "react";
import {
  AUTHORIZED_MENU_ITEMS,
  UNAUTHORIZED_MENU_ITEMS,
} from "constants/navigationConstants";
import { signOut } from "utils/token";

const Drawer = () => {
  const isBrowser = typeof window !== "undefined";
  const [drawerOpened, setDrawerOpened] = useState(false);
  const [items, setItems] = useState(UNAUTHORIZED_MENU_ITEMS);
  const [isAuthorized, setIsAuthorized] = useState(false);

  useEffect(() => {
    if (isBrowser && localStorage.getItem("token")) {
      setItems(AUTHORIZED_MENU_ITEMS);
      setIsAuthorized(true);
    } else {
      setItems(UNAUTHORIZED_MENU_ITEMS);
      setIsAuthorized(false);
    }
  }, [isBrowser && localStorage.getItem("token")]);

  const list = () => (
    <Box
      sx={{ width: 310 }}
      role="presentation"
      onClick={() => setDrawerOpened(false)}
      onKeyDown={() => setDrawerOpened(false)}
    >
      {items && (
        <List>
          {items.map((item, index) => (
            <ListItem button key={index} component="a" href={item.href}>
              <ListItemIcon>
                {index % 2 === 0 ? <Inbox /> : <Mail />}
              </ListItemIcon>
              <ListItemText primary={item.defaultMessage} />
            </ListItem>
          ))}
          {isAuthorized && (
            <ListItem button key={-1} onClick={() => signOut()}>
              <ListItemIcon>
                <Mail />
              </ListItemIcon>
              <ListItemText primary={"Wyloguj się"} />
            </ListItem>
          )}
        </List>
      )}
      {/* <Divider />
      <List>
        {["All mail", "Trash", "Spam"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <Inbox /> : <Mail />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List> */}
    </Box>
  );

  return (
    <div>
      <IconButton
        size="large"
        edge="end"
        color="inherit"
        aria-label="menu"
        onClick={() => setDrawerOpened(true)}
      >
        <MenuIcon />
      </IconButton>
      <SwipeableDrawer
        anchor="right"
        open={drawerOpened}
        onClose={() => setDrawerOpened(false)}
        onOpen={() => setDrawerOpened(true)}
      >
        {list()}
      </SwipeableDrawer>
    </div>
  );
};

export default Drawer;
