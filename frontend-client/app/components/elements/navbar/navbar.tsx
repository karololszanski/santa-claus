import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { PROJECT_TITLE } from "constants/projectConstants";
import Drawer from "../drawer/drawer";
import router from "next/dist/client/router";

const Navbar = () => {
  const redirectHome = () => {
    router.push("/");
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            onClick={redirectHome}
            sx={{ flexGrow: 1, cursor: "pointer" }}
          >
            {PROJECT_TITLE}
          </Typography>
          {/* <Button variant="outlined" color="inherit">
            {AUTH_MENU_ITEM.defaultMessage}
          </Button> */}
          <Drawer />
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
