import { Button, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { Box } from "@mui/system";
import { checkOrphanageCode } from "api/checkOrphanageCode";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { OrphanageCodeBoxStyles } from "./letterElements.style";
import { useRouter } from "next/dist/client/router";

type OrphanageProps = {
  letterDetails: any;
  setLetterDetails: (any) => void;
  handleNext: () => void;
};

const OrphangeCode: React.FC<OrphanageProps> = ({
  letterDetails,
  setLetterDetails,
  handleNext,
}) => {
  const router = useRouter();
  const { query, pathname } = router;
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState({
    value: letterDetails.orphanageCode,
    error: false,
  });

  useEffect(() => {
    if (query?.code && !code?.value) {
      const code = query?.code;
      setCode((prev) => ({ ...prev, value: code }));
      setLetterDetails((prev) => ({
        ...prev,
        orphanageCode: code as string,
      }));
    } else if (query?.code && code?.value) {
      router.push({
        pathname,
        query: null,
      });
      checkOrphanageCodeAndNext(null);
    }
  }, [query?.code, code]);

  const handleChange = (event) => {
    setCode((prev) => ({ ...prev, value: event.target.value }));
    setLetterDetails((prev) => ({
      ...prev,
      orphanageCode: event.target.value,
    }));
  };

  const isFormValid = () => {
    return code?.value && code?.value.length === 6 && /^\d+$/.test(code?.value);
  };

  const checkOrphanageCodeAndNext = (e) => {
    e?.preventDefault();
    setLoading(true);
    if (isFormValid()) {
      checkOrphanageCode(
        code.value,
        (response) => {
          setLetterDetails((prev) => ({
            ...prev,
            orphanageCode: code.value,
          }));
          handleNext();
          setLoading(false);
        },
        (error) => {
          console.log("Error: ", error?.response?.data?.msg);
          toast.error(error?.response?.data?.msg, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: error?.response?.data?.msg,
          });
          setLoading(false);
        }
      );
    } else {
      setLoading(false);
      toast.error("Sprawdź czy kod jest poprawny", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: "invalidForm",
      });
    }
  };

  return (
    <>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        sx={OrphanageCodeBoxStyles}
      >
        <TextField
          label="Kod dostępu"
          placeholder="Podaj kod dostępu"
          variant="outlined"
          type="text"
          required
          value={code.value}
          onChange={(e) => {
            handleChange(e);
          }}
        />
      </Box>
      <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
        <Box sx={{ flex: "1 1 auto" }} />
        <LoadingButton onClick={checkOrphanageCodeAndNext} loading={loading}>
          {"Następny krok"}
        </LoadingButton>
      </Box>
    </>
  );
};

export default OrphangeCode;
