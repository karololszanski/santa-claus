import { Button, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import { toast } from "react-toastify";
import {
  LetterBoxStyles,
  OrphanageCodeBoxStyles,
} from "./letterElements.style";

type LetterProps = {
  letterDetails: any;
  setLetterDetails: (any) => void;
  handleBack: () => void;
  handleNext: () => void;
};

const LetterForm: React.FC<LetterProps> = ({
  letterDetails,
  setLetterDetails,
  handleBack,
  handleNext,
}) => {
  const CHARACTER_LIMIT = 1500;
  const [letter, setLetter] = useState({ value: letterDetails.letterBody });

  const handleChange = (event) => {
    setLetter((prev) => ({ ...prev, value: event.target.value }));
    setLetterDetails((prev) => ({
      ...prev,
      letterBody: event.target.value,
    }));
  };

  const isFormValid = () => {
    return (
      letter?.value &&
      letter?.value.replace(/\n/g, "\r\n").length > 50 &&
      letter?.value.replace(/\n/g, "\r\n").length < CHARACTER_LIMIT
    );
  };

  const checkDataAndNext = (e) => {
    e.preventDefault();
    console.log("Letter length: ", letter?.value.replace(/\n/g, "\r\n").length);
    if (isFormValid()) {
      setLetterDetails((prev) => ({
        ...prev,
        letterBody: letter.value.replace(/\n/g, "\r\n"),
      }));
      handleNext();
    } else {
      toast.error("List powinien zawierać od 50 do 1500 znaków", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: "invalidForm",
      });
    }
    console.log("Do zrobienia sprawdzanie kodu jak git to następny krok");
  };

  return (
    <>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        sx={OrphanageCodeBoxStyles}
      >
        <TextField
          label="Treść listu"
          placeholder="Napisz list"
          variant="outlined"
          type="text"
          required
          multiline
          inputProps={{
            maxlength: CHARACTER_LIMIT,
          }}
          helperText={`${
            letter.value.replace(/\n/g, "\r\n").length
          }/${CHARACTER_LIMIT}`}
          value={letter.value}
          minRows={6}
          maxRows={14}
          onChange={(e) => {
            handleChange(e);
          }}
          sx={LetterBoxStyles}
        />
      </Box>
      <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
        <Button color="inherit" onClick={handleBack} sx={{ mr: 1 }}>
          Wróć
        </Button>
        <Box sx={{ flex: "1 1 auto" }} />
        <Button onClick={checkDataAndNext}>Następny krok</Button>
      </Box>
    </>
  );
};

export default LetterForm;
