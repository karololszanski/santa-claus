export const OrphanageCodeBoxStyles: any = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  padding: {
    xs: "30px 10px",
    sm: "30px 20px",
  },
};

export const LetterBoxStyles: any = {
  width: {
    xs: "95%",
    sm: "80%",
  },
};
