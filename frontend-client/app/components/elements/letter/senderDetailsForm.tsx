import { Person } from "@mui/icons-material";
import { Button, IconButton, InputAdornment, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import { AuthTextFieldStyles } from "../authentication/authentication.styles";
import { checkTextLength } from "../authentication/authHelpFunctions";
import { OrphanageCodeBoxStyles } from "./letterElements.style";

type SenderDetailsFormProps = {
  letterDetails: any;
  setLetterDetails: (any) => void;
  handleBack: () => void;
  handleNext: () => void;
};

const SenderDetailsForm: React.FC<SenderDetailsFormProps> = ({
  letterDetails,
  setLetterDetails,
  handleBack,
  handleNext,
}) => {
  const [values, setValues] = useState({
    firstName: {
      value: letterDetails.firstName,
      error: "",
      init: false,
    },
    lastName: {
      value: letterDetails.lastName,
      error: "",
      init: false,
    },
  });

  const handleChange =
    (prop, error, init = false) =>
    (event) => {
      setValues({
        ...values,
        [prop]: {
          ...values[prop],
          value: event.target.value,
          error: error,
          init: init ? true : values[prop].init,
        },
      });
      setLetterDetails((prev) => ({
        ...prev,
        [prop]: event.target.value,
      }));
    };

  const isFormValid = () => {
    return !(
      !values.firstName.value! ||
      values.firstName.value.length < 2 ||
      values.firstName.value.length > 64 ||
      !values.lastName.value! ||
      values.lastName.value.length < 2 ||
      values.lastName.value.length > 64
    );
  };

  const checkDataAndNext = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      setLetterDetails((prev) => ({
        ...prev,
        firstName: values.firstName.value,
        lastName: values.lastName.value,
      }));
      console.log(values.firstName, values.lastName);
      handleNext();
    } else {
      console.log("Form not valid");
    }
    console.log("Do zrobienia sprawdzanie kodu jak git to następny krok");
  };

  return (
    <>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        sx={OrphanageCodeBoxStyles}
      >
        <TextField
          label="Imię"
          placeholder="Imię"
          variant="outlined"
          required
          value={values.firstName.value}
          error={values.firstName.init && values.firstName.error.length > 0}
          helperText={values.firstName.init && values.firstName.error}
          onBlur={(e) => {
            handleChange(
              "firstName",
              checkTextLength(e.target.placeholder, 2, 64)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "firstName",
              checkTextLength(e.target.placeholder, 2, 64)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Person />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Nazwisko"
          placeholder="Nazwisko"
          variant="outlined"
          required
          value={values.lastName.value}
          error={values.lastName.init && values.lastName.error.length > 0}
          helperText={values.lastName.init && values.lastName.error}
          onBlur={(e) => {
            handleChange(
              "lastName",
              checkTextLength(e.target.placeholder, 2, 10)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "lastName",
              checkTextLength(e.target.placeholder, 2, 10)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Person />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
        <Button color="inherit" onClick={handleBack} sx={{ mr: 1 }}>
          Wróć
        </Button>
        <Box sx={{ flex: "1 1 auto" }} />
        <Button onClick={checkDataAndNext}>Następny krok</Button>
      </Box>
    </>
  );
};

export default SenderDetailsForm;
