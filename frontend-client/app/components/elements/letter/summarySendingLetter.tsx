import { Person } from "@mui/icons-material";
import { LoadingButton } from "@mui/lab";
import {
  Button,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
} from "@mui/material";
import { Box } from "@mui/system";
import { sendLetter } from "api/letter/sendLetter";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { AuthTextFieldStyles } from "../authentication/authentication.styles";
import {
  LetterBoxStyles,
  OrphanageCodeBoxStyles,
} from "./letterElements.style";

type LetterProps = {
  letterDetails: any;
  handleBack: () => void;
  handleNext: () => void;
};

const SummarySendingLetter: React.FC<LetterProps> = ({
  letterDetails,
  handleBack,
  handleNext,
}) => {
  const [loading, setLoading] = useState(false);
  const sendLetterAndNext = () => {
    setLoading(true);
    sendLetter(
      letterDetails.firstName,
      letterDetails.lastName,
      letterDetails.orphanageCode,
      letterDetails.letterBody,
      (response) => {
        handleNext();
        setLoading(false);
      },
      (error) => {
        toast.error("Wystąpił błąd. Sprawdź formularz", {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: "invalidForm",
        });
        setLoading(false);
      }
    );
  };
  return (
    <>
      <Grid container spacing={2} sx={{ my: 4 }}>
        <Grid item sx={OrphanageCodeBoxStyles} xs={12} sm={6}>
          <TextField
            label="Imię"
            placeholder="Imię"
            variant="outlined"
            required
            disabled
            value={letterDetails.firstName}
            sx={AuthTextFieldStyles}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconButton aria-label="email">
                    <Person />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <TextField
            label="Nazwisko"
            placeholder="Nazwisko"
            variant="outlined"
            required
            disabled
            value={letterDetails.lastName}
            sx={AuthTextFieldStyles}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconButton aria-label="email">
                    <Person />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            label="Treść listu"
            placeholder="Napisz list"
            variant="outlined"
            type="text"
            required
            multiline
            disabled
            value={letterDetails.letterBody}
            minRows={6}
            maxRows={14}
            sx={LetterBoxStyles}
          />
        </Grid>
      </Grid>
      <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
        <Button color="inherit" onClick={handleBack} sx={{ mr: 1 }}>
          Wróć
        </Button>
        <Box sx={{ flex: "1 1 auto" }} />
        <LoadingButton onClick={sendLetterAndNext} loading={loading}>
          Wyślij
        </LoadingButton>
      </Box>
    </>
  );
};

export default SummarySendingLetter;
