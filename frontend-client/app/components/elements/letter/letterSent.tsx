import { Button, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

type LetterSentProps = {
  handleReset: () => void;
};

const LetterSent: React.FC<LetterSentProps> = ({ handleReset }) => {
  return (
    <>
      <Typography
        variant="h1"
        sx={{ fontSize: 24, fontWeight: 300, my: 5 }}
        align="center"
        gutterBottom
        component="div"
      >
        List został pomyślnie wysłany!
      </Typography>
      <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
        <Box sx={{ flex: "1 1 auto" }} />
        <Button onClick={handleReset}>Reset</Button>
      </Box>
    </>
  );
};

export default LetterSent;
