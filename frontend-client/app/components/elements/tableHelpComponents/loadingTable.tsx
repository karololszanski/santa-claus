import React from "react";
import { Box, CircularProgress } from "@mui/material";

export const LoadingTable = () => {
  return (
    <Box
      sx={{
        width: "100%",
        height: "300px",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress />
    </Box>
  );
};

export default LoadingTable;
