import React from "react";
import WebAssetOffIcon from "@mui/icons-material/WebAssetOff";
import { Box, Typography } from "@mui/material";

export const NoLetterFound = () => {
  return (
    <Box
      sx={{
        width: "100%",
        height: "300px",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          width: "100%",
          height: "300px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <WebAssetOffIcon sx={{ m: "0 auto", fontSize: "40px" }} />
        <Typography
          sx={{
            m: "0 auto",
            textAlign: "center",
            //   verticalAlign: "middle",
            //   lineHeight: "300px",
            fontSize: "20px",
          }}
        >
          Nie znaleziono żadnych listów
        </Typography>
      </Box>
    </Box>
  );
};

export default NoLetterFound;
