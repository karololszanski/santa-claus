import {
  Avatar,
  Box,
  Button,
  Chip,
  Collapse,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { stringAvatar } from "./browserHelpFunctions";
import { connectLetterWithUser } from "api/letter/connectLetterWithUser";
import { checkToken } from "utils/token";
import NoLetterFound from "../tableHelpComponents/noLetterFound";
import LoadingTable from "../tableHelpComponents/loadingTable";
import router from "next/dist/client/router";
import { LETTERS_PAGE } from "constants/navigationConstants";

type LettersTableComponentProps = {
  data: any;
  selectedItems: any;
  selectedLocation: any;
};

const LettersTableComponent: React.FC<LettersTableComponentProps> = ({
  data,
  selectedItems,
  selectedLocation,
}) => {
  const [letters, setLetters] = useState(null);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
    if (data) {
      setLetters(data);
    }
  }, [data]);

  useEffect(() => {
    if (data) {
      let filteredData = data;
      if (selectedItems?.length > 0) {
        filteredData = filteredData.filter((letter) => {
          return selectedItems
            .map((item) => item?.name)
            .includes(letter?.category?.name);
        });
      }
      if (selectedLocation) {
        filteredData = filteredData.filter((letter) => {
          if (selectedLocation.region) {
            return (
              letter.orphanage.voivodeship === selectedLocation.voivodeship
            );
          } else {
            return letter.orphanage.name === selectedLocation.orphanage;
          }
        });
      }
      setLetters(filteredData);
    }
  }, [selectedItems, selectedLocation]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const connectLetterWithUserAction = (letter_id) => {
    connectLetterWithUser(
      letter_id,
      (response) => {
        setLetters(letters.filter((letter) => letter?.uuid !== letter_id));
        router.push(LETTERS_PAGE);
      },
      (error) => {
        if (error?.response?.status === 401) {
          setTimeout(() => {
            if (error?.response?.status === 401 && checkToken()) {
              setLetters(
                letters.filter((letter) => letter?.uuid !== letter_id)
              );
              router.push(LETTERS_PAGE);
            }
          }, 1000);
        } else {
          console.log("Error łączenia listu z userem: ", { error });
        }
      }
    );
  };

  const Row = ({ letter }) => {
    const [open, setOpen] = useState(letter?.open ?? false);

    return (
      <>
        {letter?.step === 2 && (
          <>
            <TableRow
              sx={{
                "& > *": {
                  borderBottom: "unset",
                  bgcolor: open ? "primary.light" : "white",
                },
              }}
              onClick={() => setOpen(!open)}
            >
              <TableCell size="small" sx={{ width: "20px", p: 0 }}>
                <IconButton
                  aria-label="expand row"
                  size="small"
                  sx={{ color: open ? "white" : "text.primary" }}
                >
                  {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
              </TableCell>
              <TableCell>
                <Grid container sx={{ width: "100%" }}>
                  <Grid
                    item
                    xs={6}
                    sx={{ display: "flex", alignItems: "center" }}
                  >
                    <Avatar
                      {...stringAvatar(
                        `${letter.first_name} ${letter.last_name}`
                      )}
                    />
                    <Typography
                      align="center"
                      component="div"
                      sx={{ mx: 2, color: open ? "white" : "text.primary" }}
                    >{`${letter.first_name} ${letter.last_name}.`}</Typography>
                  </Grid>
                  <Grid container item xs={6} spacing={1}>
                    <Grid item xs={12}>
                      <Chip
                        icon={<LocationOnIcon />}
                        label={letter?.orphanage?.name}
                        variant="outlined"
                        sx={{ bgcolor: "white" }}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sx={{ color: open ? "white" : "text.primary" }}
                    >
                      {/* <Chip
                    label={letter.k}
                    variant="outlined"
                    sx={{ bgcolor: "white" }}
                  />{" "}
                  {` / `} */}
                      <Chip
                        label={letter?.category?.name}
                        sx={{
                          fontWeight: 500,
                          bgcolor: "success.dark",
                          color: "white",
                        }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                style={{ paddingBottom: 0, paddingTop: 0 }}
                colSpan={6}
              >
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <Box
                    sx={{
                      alignItems: "center",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Paper
                      elevation={6}
                      sx={{
                        m: 1,
                        transform: "rotate(359deg)",
                        bgcolor: "#fffce8",
                      }}
                    >
                      <Typography
                        component="div"
                        sx={{
                          p: 2,
                          whiteSpace: "pre-line",
                          fontFamily: "Caveat, cursive",
                          fontSize: "24px",
                          maxHeight: "500px",
                          overflow: "auto",
                        }}
                      >
                        {letter.text}
                      </Typography>
                    </Paper>
                    <Button
                      variant="contained"
                      color="primary"
                      sx={{ m: 2 }}
                      onClick={() => connectLetterWithUserAction(letter?.uuid)}
                    >
                      Zaaplikuj i zostań św. Mikołajem
                    </Button>
                  </Box>
                </Collapse>
              </TableCell>
            </TableRow>
          </>
        )}
      </>
    );
  };

  return (
    <Paper>
      <TableContainer>
        <Table aria-label="letters-table">
          <TableBody>
            {letters ? (
              letters?.length > 0 ? (
                letters
                  ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((letter, index) => (
                    <Row key={letter.uuid} letter={letter} />
                  ))
              ) : (
                <NoLetterFound />
              )
            ) : (
              <LoadingTable />
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={letters?.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelRowsPerPage={"Listów na stronie"}
        labelDisplayedRows={function defaultLabelDisplayedRows({
          from,
          to,
          count,
        }) {
          return `${from}-${to} z ${count !== -1 ? count : `więcej niż ${to}`}`;
        }}
      />
    </Paper>
  );
};

export default LettersTableComponent;
