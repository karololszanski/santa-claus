import { Autocomplete, Grid, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { prepareItems, prepareLocalizations } from "./browserHelpFunctions";

type SearchComponentProps = {
  letters: any;
  selectedItems: any;
  setSelectedItems: (any) => void;
  localizations: any;
  setLocalizations: (any) => void;
  selectedLocation: any;
  setSelectedLocation: (any) => void;
};

const SearchComponent: React.FC<SearchComponentProps> = ({
  letters,
  selectedItems,
  setSelectedItems,
  localizations,
  setLocalizations,
  selectedLocation,
  setSelectedLocation,
}) => {
  const [labels, setLabels] = useState({
    items: "Wszystkie przedmioty",
    location: "Cała Polska",
  });
  const [items, setItems] = useState(null);

  useEffect(() => {
    if (letters) {
      setItems(prepareItems(letters));
      setLocalizations(prepareLocalizations(letters));
    }
  }, [letters]);

  const onItemChange = (e, value) => {
    console.log("Event: ", e, " Value: ", value);
    setSelectedItems(value);
  };
  const onLocationChange = (e, value) => {
    console.log("Event: ", e, " Value: ", value);
    setSelectedLocation(value);
  };
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        {items && (
          <Autocomplete
            multiple
            limitTags={3}
            options={items}
            groupBy={(item) => item.parent}
            getOptionLabel={(item) => item.name}
            onChange={onItemChange}
            onFocus={() => {
              setLabels((prev) => ({ ...prev, items: "Przedmioty" }));
            }}
            onBlur={() => {
              !selectedItems.length &&
                setLabels((prev) => ({
                  ...prev,
                  items: "Wszystkie przedmioty",
                }));
            }}
            isOptionEqualToValue={(option, value) => {
              return JSON.stringify(option) === JSON.stringify(value);
            }}
            fullWidth
            renderInput={(params) => (
              <TextField
                {...params}
                label={labels.items}
                placeholder={selectedItems.length ? "" : "Wybierz przedmioty"}
              />
            )}
          />
        )}
      </Grid>
      <Grid item xs={12} sm={6}>
        {localizations && (
          <Autocomplete
            options={localizations}
            groupBy={(option) => option.voivodeship}
            getOptionLabel={(option) => option.orphanage}
            onChange={onLocationChange}
            value={selectedLocation}
            onFocus={() => {
              setLabels((prev) => ({ ...prev, location: "Lokalizacja" }));
            }}
            onBlur={() => {
              !selectedLocation &&
                setLabels((prev) => ({ ...prev, location: "Cała Polska" }));
            }}
            isOptionEqualToValue={(option, value) => {
              return JSON.stringify(option) === JSON.stringify(value);
            }}
            fullWidth
            renderInput={(params) => (
              <TextField
                {...params}
                label={labels.location}
                placeholder="Wybierz lokalizacje"
              />
            )}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default SearchComponent;
