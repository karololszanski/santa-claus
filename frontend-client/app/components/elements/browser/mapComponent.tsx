import { Box } from "@mui/system";
import React, { useEffect, useRef, useState } from "react";
import { Map, Marker, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";

const blueMarker =
  "https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color/254000/66-512.png";
const redMarker =
  "https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color/254000/67-512.png";

const LocalizationIcon = L.icon({
  iconUrl: blueMarker,
  iconRetinaUrl: blueMarker,
  iconAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [50, 50],
  className: "leaflet-venue-icon",
});

const SelectedLocalizationIcon = L.icon({
  iconUrl: redMarker,
  iconRetinaUrl: redMarker,
  iconAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [50, 50],
  className: "leaflet-venue-icon",
});

type MapComponentProps = {
  localizations: any;
  selectedLocation: any;
  setSelectedLocation: (any) => void;
};

const MapComponent: React.FC<MapComponentProps> = ({
  localizations,
  selectedLocation,
  setSelectedLocation,
}) => {
  const mapRef = useRef(null);
  const [center, setCenter] = useState([51.9358387, 19.2231721]);
  const [zoom, setZoom] = useState<number>(6);

  useEffect(() => {
    console.log("Selected location: ", selectedLocation);
    if (selectedLocation && selectedLocation?.lat && selectedLocation?.lng) {
      setCenter([selectedLocation?.lat, selectedLocation?.lng]);
      setZoom(selectedLocation.region ? 8 : 10);
    } else {
      setCenter([51.9358387, 19.2231721]);
      setZoom(6);
    }
  }, [selectedLocation]);

  useEffect(() => {
    localizations && console.log("Localizations: ", localizations);
  }, [localizations]);

  return (
    <Box
      sx={{
        width: "100%",
        height: "100%",
        overflow: "hidden",
        ".leaflet-container": {
          height: "300px",
          width: "300px",
        },
      }}
    >
      <Map
        style={{ height: "100%", width: "100%" }}
        center={center}
        scrollWheelZoom={true}
        zoom={zoom}
        ref={mapRef}
        onViewportChange={() => setZoom(mapRef.current.viewport.zoom)}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {localizations?.map((localization, index) => {
          return (
            !localization.region &&
            localization !== selectedLocation &&
            localization?.lat &&
            localization?.lng && (
              <Marker
                key={index}
                position={[localization?.lat, localization?.lng]}
                icon={LocalizationIcon}
                onClick={() => setSelectedLocation(localization)}
              />
            )
          );
        })}
        {selectedLocation &&
          !selectedLocation.region &&
          selectedLocation?.lat &&
          selectedLocation?.lng && (
            <Marker
              position={[selectedLocation?.lat, selectedLocation?.lng]}
              icon={SelectedLocalizationIcon}
              onClick={() => setSelectedLocation(null)}
            />
          )}
      </Map>
    </Box>
  );
};

export default MapComponent;
