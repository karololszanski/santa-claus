export const prepareItems = (letters: any) => {
  const mappedItems = letters
    .map((item) => {
      return { name: item?.category?.name, parent: item?.category?.parent };
    })
    .filter(
      (item, index, self) =>
        index === self.findIndex((element) => item.name === element.name)
    )
    .sort((a, b) => {
      if (a.item < b.item) {
        return -1;
      }
      if (a.item > b.item) {
        return 1;
      }
      return 0;
    })
    .sort((a, b) => {
      if (a.parent < b.parent) {
        return -1;
      }
      if (a.parent > b.parent) {
        return 1;
      }
      return 0;
    });

  return mappedItems;
};

const getCenter = (letters) => {
  let minX, maxX, minY, maxY;
  letters.forEach((letter) => {
    minX =
      +(letter as any)?.lat < minX || minX == null
        ? +(letter as any)?.lat
        : minX;
    maxX =
      +(letter as any)?.lat > maxX || maxX == null
        ? +(letter as any)?.lat
        : maxX;
    minY =
      +(letter as any)?.lng < minY || minY == null
        ? +(letter as any)?.lng
        : minY;
    maxY =
      +(letter as any)?.lng > maxY || maxY == null
        ? +(letter as any)?.lng
        : maxY;
  });
  return [(minX + maxX) / 2, (minY + maxY) / 2];
};

export const prepareLocalizations = (letters: any) => {
  // Sortowanie po nazwie
  const lettersSorted = letters
    .map((letter) => {
      return {
        voivodeship: letter.orphanage.voivodeship,
        orphanage: letter.orphanage.name,
        lat: letter.orphanage.lat,
        lng: letter.orphanage.lng,
        region: false,
      };
    })
    .sort((a, b) => {
      if (a.orphanage < b.orphanage) {
        return -1;
      }
      if (a.orphanage > b.orphanage) {
        return 1;
      }
      return 0;
    });
  // Stworzenie listy województw
  const voivodeshipes = [];
  for (const item of lettersSorted) {
    if (
      !voivodeshipes.some((element) => {
        return element.voivodeship === item.voivodeship;
      })
    ) {
      const lettersFromVoivodeship = lettersSorted.filter(
        (letter) => letter.voivodeship === item.voivodeship
      );
      const orphanagesFromVoivodeship = lettersFromVoivodeship.filter(
        (item, index, self) =>
          index ===
          self.findIndex((element) => item.orphanage === element.orphanage)
      );
      if (orphanagesFromVoivodeship.length > 1) {
        // Jeżeli tylko 1 dom dziecka w województwie to nie dodajemy opcji "Całe {województwo}"
        const center = getCenter(lettersFromVoivodeship);
        voivodeshipes.push({
          voivodeship: item.voivodeship,
          orphanage: `Całe ${item.voivodeship}`,
          lat: center[0],
          lng: center[1],
          region: true,
        });
      }
    }
  }

  const resultArraySortedByVoivodeshipes = voivodeshipes
    .concat(lettersSorted)
    .filter(
      (item, index, self) =>
        index ===
        self.findIndex((element) => item.orphanage === element.orphanage)
    )
    .sort((a, b) => {
      if (a.voivodeship < b.voivodeship) {
        return -1;
      }
      if (a.voivodeship > b.voivodeship) {
        return 1;
      }
      return 0;
    });
  return resultArraySortedByVoivodeshipes;
};

export const stringToColor = (string: string) => {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.substr(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
};

export const stringAvatar = (name: string) => {
  return {
    sx: {
      bgcolor: stringToColor(name),
    },
    children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
  };
};
