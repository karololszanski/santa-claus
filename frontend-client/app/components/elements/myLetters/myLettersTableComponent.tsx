import {
  Avatar,
  Box,
  Button,
  Chip,
  CircularProgress,
  Collapse,
  Grid,
  IconButton,
  Paper,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { stringAvatar } from "../browser/browserHelpFunctions";
import { updateStep } from "api/letter/updateLetterStep";
import letter from "../../../../pages/letter";
import { checkToken } from "utils/token";
import NoLetterFound from "../tableHelpComponents/noLetterFound";
import LoadingTable from "../tableHelpComponents/loadingTable";

type MyLettersTableComponentProps = {
  data: any;
};

const MyLettersTableComponent: React.FC<MyLettersTableComponentProps> = ({
  data,
}) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [formattedData, setFormattedData] = useState(null);

  useEffect(() => {
    if (data) {
      setFormattedData(data);
    }
  }, [data]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleNextStep = (letter_id, step) => {
    updateStep(
      letter_id,
      step,
      (response) => {
        console.log("Otrzymałem response: ", response);
        setFormattedData(
          step < 3
            ? formattedData.filter((letter) => letter?.uuid !== letter_id)
            : formattedData.map((letter) => {
                if (letter?.uuid === letter_id) {
                  return { ...letter, step, open: true };
                } else {
                  return letter;
                }
              })
        );
      },
      (error) => {
        if (error?.response?.status === 401) {
          setTimeout(() => {
            if (error?.response?.status === 401 && checkToken()) {
              setFormattedData(
                step < 3
                  ? formattedData.filter((letter) => letter?.uuid !== letter_id)
                  : formattedData.map((letter) => {
                      if (letter?.uuid === letter_id) {
                        return { ...letter, step, open: true };
                      } else {
                        return letter;
                      }
                    })
              );
            }
          }, 1000);
        } else {
          console.log("Error updatowanie kroku: ", { error });
        }
      }
    );
  };

  const Row = ({ letter }) => {
    const [open, setOpen] = useState(letter?.open ?? false);

    return (
      <>
        {letter?.step > 2 && (
          <>
            <TableRow
              sx={{
                "& > *": {
                  borderBottom: "unset",
                  bgcolor: open ? "primary.light" : "white",
                },
              }}
              onClick={() => setOpen(!open)}
            >
              <TableCell size="small" sx={{ width: "20px", p: 0 }}>
                <IconButton
                  aria-label="expand row"
                  size="small"
                  sx={{ color: open ? "white" : "text.primary" }}
                >
                  {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                </IconButton>
              </TableCell>
              <TableCell>
                <Grid container sx={{ width: "100%" }}>
                  <Grid
                    item
                    xs={6}
                    sx={{ display: "flex", alignItems: "center" }}
                  >
                    <Avatar
                      {...stringAvatar(
                        `${letter.first_name} ${letter.last_name}`
                      )}
                    />
                    <Typography
                      align="center"
                      component="div"
                      sx={{ mx: 2, color: open ? "white" : "text.primary" }}
                    >{`${letter.first_name} ${letter.last_name}`}</Typography>
                  </Grid>
                  <Grid container item xs={6} spacing={1}>
                    <Grid item xs={12}>
                      <Chip
                        icon={<LocationOnIcon />}
                        label={letter?.orphanage?.name}
                        variant="outlined"
                        sx={{ bgcolor: "white" }}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sx={{ color: open ? "white" : "text.primary" }}
                    >
                      {/* <Chip
                    label={letter.k}
                    variant="outlined"
                    sx={{ bgcolor: "white" }}
                  />{" "}
                  {` / `} */}
                      <Chip
                        label={letter?.category?.name}
                        sx={{
                          fontWeight: 500,
                          bgcolor: "success.dark",
                          color: "white",
                        }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell
                style={{ paddingBottom: 0, paddingTop: 0 }}
                colSpan={6}
              >
                <Collapse in={open} timeout="auto" unmountOnExit>
                  <Box
                    sx={{
                      alignItems: "center",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <Grid container>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: { xs: "center", sm: "flex-end" },
                          padding: "30px 20px",
                        }}
                      >
                        <TextField
                          label="Nazwa domu dziecka"
                          variant="standard"
                          disabled
                          value={letter?.orphanage?.name}
                          sx={{ margin: "5px" }}
                        />
                        <TextField
                          label="Telefon"
                          variant="standard"
                          disabled
                          value={letter?.orphanage?.phone}
                          sx={{ margin: "5px" }}
                        />
                        <TextField
                          label="Email"
                          variant="standard"
                          disabled
                          value={letter?.orphanage?.email}
                          sx={{ margin: "5px" }}
                        />
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: { xs: "center", sm: "flex-start" },
                          padding: "30px 20px",
                        }}
                      >
                        <TextField
                          label="Adres domu dziecka"
                          variant="standard"
                          multiline
                          disabled
                          value={`${letter?.orphanage?.name}\r\nul. ${letter?.orphanage?.street} ${letter?.orphanage?.building_number}\r\n${letter?.orphanage?.postal_code} ${letter?.orphanage?.city}\r\n${letter?.orphanage?.voivodeship}`}
                          sx={{ margin: "5px" }}
                        />
                      </Grid>
                    </Grid>
                    {letter?.step === 3 ? (
                      <>
                        <Button
                          variant="contained"
                          color="success"
                          sx={{ m: 2 }}
                          onClick={() =>
                            handleNextStep(letter?.uuid, letter?.step + 1)
                          }
                        >
                          Potwierdź wysłanie paczki
                        </Button>
                        <Button
                          variant="contained"
                          color="error"
                          sx={{ m: 2 }}
                          onClick={() =>
                            handleNextStep(letter?.uuid, letter?.step - 1)
                          }
                        >
                          Porzuć list
                        </Button>
                      </>
                    ) : (
                      <Stepper
                        activeStep={letter?.step - 2}
                        orientation="horizontal"
                        sx={{ m: 2 }}
                        alternativeLabel={true}
                      >
                        {[
                          {
                            label: "Wybranie listu",
                            description: "List został przez Ciebie wybrany",
                          },
                          {
                            label: "Wysłanie paczki",
                            description:
                              "Paczka została przez Ciebie wysłana. Obecnie dom dziecka oczeukje przesyłki",
                          },
                          {
                            label: "Przesyłka dotarła!",
                            description:
                              "Hura! Paczka została dostarczona do wskazanego domu dziecka",
                          },
                        ].map((step, index) => (
                          <Step key={index}>
                            <StepLabel>{step.label}</StepLabel>
                            {/* <StepContent>
                          <Typography>{step.description}</Typography>
                        </StepContent> */}
                          </Step>
                        ))}
                      </Stepper>
                    )}
                    <Paper
                      elevation={6}
                      sx={{
                        m: 1,
                        transform: "rotate(359deg)",
                        bgcolor: "#fffce8",
                      }}
                    >
                      <Typography
                        component="div"
                        sx={{
                          p: 2,
                          whiteSpace: "pre-line",
                          fontFamily: "Caveat, cursive",
                          fontSize: "24px",
                          maxHeight: "500px",
                          overflow: "auto",
                        }}
                      >
                        {letter.text}
                      </Typography>
                    </Paper>
                  </Box>
                </Collapse>
              </TableCell>
            </TableRow>
          </>
        )}
      </>
    );
  };

  return (
    <Paper>
      <TableContainer>
        <Table aria-label="letters-table">
          <TableBody>
            {formattedData ? (
              formattedData?.length > 0 ? (
                formattedData
                  ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  ?.map((letter) => <Row key={letter.uuid} letter={letter} />)
              ) : (
                <NoLetterFound />
              )
            ) : (
              <LoadingTable />
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={formattedData?.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelRowsPerPage={"Listów na stronie"}
        labelDisplayedRows={function defaultLabelDisplayedRows({
          from,
          to,
          count,
        }) {
          return `${from}-${to} z ${count !== -1 ? count : `więcej niż ${to}`}`;
        }}
      />
    </Paper>
  );
};

export default MyLettersTableComponent;
