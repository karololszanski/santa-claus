import React, { useState } from "react";
import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
  AuthBoxStyles,
  AuthButtonStyles,
  AuthTextFieldStyles,
} from "../authentication.styles";
import { checkTextLength, checkEmail } from "../authHelpFunctions";
import { Email, Lock, Person } from "@mui/icons-material";
import router, { useRouter } from "next/dist/client/router";
import {
  HOME_PAGE,
  REGISTER_QUERY,
  RESET_PASSWORD_QUERY,
} from "../../../../constants/navigationConstants";
import { login } from "api/auth/login";
import { toast } from "react-toastify";
import { saveTokenCredentials } from "utils/token";
import axios from "axios";

const LoginForm = () => {
  const { query } = useRouter();
  const [values, setValues] = useState({
    email: {
      value: "",
      error: "",
      init: false,
    },
    password: {
      value: "",
      error: "",
      showPassword: false,
      init: false,
    },
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      console.log(values.email, values.password);
      login(
        values.email.value,
        values.password.value,
        (response) => {
          console.log(response);
          saveTokenCredentials(response);
          axios.defaults.headers.common.authorization = `Bearer ${
            JSON.parse(localStorage.getItem("token")).access_token
          }`;
          router.push(HOME_PAGE);
        },
        (error) => {
          toast.error(error?.response?.data?.msg, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: error?.response?.data?.msg,
          });
        }
      );
    } else {
      toast.error("Sprawdź czy wszytskie pola są poprawnie wypełnione", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: "invalidForm",
      });
    }
  };

  const handleChange =
    (prop, error, init = false) =>
    (event) => {
      setValues({
        ...values,
        [prop]: {
          ...values[prop],
          value: event.target.value,
          error: error,
          init: init ? true : values[prop].init,
        },
      });
    };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      password: {
        ...values.password,
        showPassword: !values.password.showPassword,
      },
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const redirectRegister = () => {
    router.push({
      query: { ...query, type: REGISTER_QUERY },
    });
  };

  const redirectReset = () => {
    router.push({
      query: { ...query, type: RESET_PASSWORD_QUERY },
    });
  };

  const isFormValid = () => {
    return !(
      !values.email.value ||
      checkEmail(values.email.value).length > 0 ||
      !values.password.value ||
      values.password.value.length < 8 ||
      values.password.value.length > 128
    );
  };

  return (
    <>
      <Typography
        variant="h4"
        sx={{ fontWeight: 300, mt: 5 }}
        align="center"
        gutterBottom
        component="div"
      >
        Zostań Mikołajem już dziś
      </Typography>
      <Box
        sx={AuthBoxStyles}
        component="form"
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <TextField
          label="Email"
          placeholder="Email"
          variant="outlined"
          type="email"
          required
          value={values.email.value}
          error={values.email.init && values.email.error.length > 0}
          helperText={values.email.init && values.email.error}
          onBlur={(e) => {
            handleChange("email", checkEmail(e.target.value), true)(e);
          }}
          onChange={(e) => {
            handleChange("email", checkEmail(e.target.value))(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Email />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Hasło"
          placeholder="Hasło"
          type={values.password.showPassword ? "text" : "password"}
          required
          value={values.password.value}
          error={values.password.init && values.password.error.length > 0}
          helperText={values.password.init && values.password.error}
          onBlur={(e) => {
            handleChange(
              "password",
              checkTextLength(e.target.placeholder, 8, 128)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "password",
              checkTextLength(e.target.placeholder, 8, 128)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Lock />
                </IconButton>
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.password.showPassword ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <Box>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={AuthButtonStyles}
          >
            Zaloguj się
          </Button>
        </Box>
        <Box
          sx={{
            // borderBottom: "1px solid",
            // borderColor: "secondary.dark",
            pb: 1,
          }}
        >
          <Typography variant="h6" align="center" gutterBottom component="div">
            Nie masz konta?
            <Link
              onClick={redirectRegister}
              color="primary.dark"
              underline="hover"
              sx={{ cursor: "pointer" }}
            >
              Zarejestruj się
            </Link>
          </Typography>
        </Box>
        {/* <Box sx={{ pt: 1 }}>
          <Typography variant="h6" align="center" gutterBottom component="div">
            <Link
              onClick={redirectReset}
              color="primary.dark"
              underline="hover"
              sx={{ cursor: "pointer" }}
            >
              Zapomniałeś hasła?
            </Link>
          </Typography>
        </Box> */}
      </Box>
    </>
  );
};

export default LoginForm;
