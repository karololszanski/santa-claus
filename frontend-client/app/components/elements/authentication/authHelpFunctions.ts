export const checkEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase())
    ? ""
    : "Nieprawidłowy adres email";
};

export const checkTextLength =
  (prop: string, minLength: number, maxLength: number) => (event) => {
    if (event.target.value.length < minLength) {
      return `${prop} musi zawierać co najmniej ${minLength} ${
        minLength < 5 ? (minLength === 1 ? "znak" : "znaki") : "znaków"
      }`;
    } else if (event.target.value.length > maxLength) {
      return `${prop} musi zawierać najwyżej ${maxLength} znaki`;
    }
    return "";
  };
