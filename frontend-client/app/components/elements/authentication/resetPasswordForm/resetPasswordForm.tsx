import React, { useState } from "react";
import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import {
  AuthBoxStyles,
  AuthButtonStyles,
  AuthTextFieldStyles,
} from "../authentication.styles";
import { checkEmail } from "../authHelpFunctions";
import { Email } from "@mui/icons-material";
import router, { useRouter } from "next/dist/client/router";
import { toast } from "react-toastify";

const ResetPasswordForm = () => {
  const { query } = useRouter();
  const [values, setValues] = useState({
    email: {
      value: "",
      error: "",
      init: false,
    },
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      console.log(values.email);
    } else {
      toast.error("Sprawdź czy wszytskie pola są poprawnie wypełnione", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: "invalidForm",
      });
    }
  };

  const handleChange =
    (prop, error, init = false) =>
    (event) => {
      console.log("Values: ", values, error);
      setValues({
        ...values,
        [prop]: {
          ...values[prop],
          value: event.target.value,
          error: error,
          init: init ? true : values[prop].init,
        },
      });
    };

  const redirectBack = () => {
    router.back();
  };

  const isFormValid = () => {
    return !(!values.email.value || checkEmail(values.email.value).length > 0);
  };

  return (
    <>
      <Typography
        variant="h4"
        sx={{ fontWeight: 300, mt: 5 }}
        align="center"
        gutterBottom
        component="div"
      >
        Zresetuj hasło
      </Typography>
      <Box
        sx={AuthBoxStyles}
        component="form"
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <TextField
          label="Email"
          placeholder="Email"
          variant="outlined"
          type="email"
          required
          value={values.email.value}
          error={values.email.init && values.email.error.length > 0}
          helperText={values.email.init && values.email.error}
          onBlur={(e) => {
            handleChange("email", checkEmail(e.target.value), true)(e);
          }}
          onChange={(e) => {
            handleChange("email", checkEmail(e.target.value))(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Email />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <Box>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={AuthButtonStyles}
          >
            Zresetuj hasło
          </Button>
        </Box>
        <Box sx={{ pt: 1 }}>
          <Typography variant="h6" align="center" gutterBottom component="div">
            <Link
              onClick={redirectBack}
              color="primary.dark"
              underline="hover"
              sx={{ cursor: "pointer" }}
            >
              Wróć
            </Link>
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export default ResetPasswordForm;
