export const AuthBoxStyles: any = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  padding: "10px",
};

export const AuthButtonStyles: any = {
  margin: "10px",
};

export const AuthTextFieldStyles: any = {
  margin: "5px",
  width: "300px",
};
