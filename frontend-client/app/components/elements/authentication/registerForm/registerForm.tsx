import React, { useState } from "react";
import {
  Box,
  Button,
  IconButton,
  InputAdornment,
  Link,
  TextField,
  Typography,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
  AuthBoxStyles,
  AuthButtonStyles,
  AuthTextFieldStyles,
} from "../authentication.styles";
import { checkTextLength, checkEmail } from "../authHelpFunctions";
import { Email, Lock, Person, Phone } from "@mui/icons-material";
import router, { useRouter } from "next/dist/client/router";
import {
  HOME_PAGE,
  LOGIN_QUERY,
  RESET_PASSWORD_QUERY,
} from "../../../../constants/navigationConstants";
import axios from "axios";
import { saveTokenCredentials } from "utils/token";
import { register } from "api/auth/register";
import { toast } from "react-toastify";
import {
  AsYouType,
  parsePhoneNumber,
  getCountries,
  isPossiblePhoneNumber,
  isValidPhoneNumber,
} from "libphonenumber-js/max";

const RegisterForm = () => {
  const { query } = useRouter();
  const [temporaryNumber, setTemporaryNumber] = useState("");
  const [values, setValues] = useState({
    firstName: {
      value: "",
      error: "",
      init: false,
    },
    lastName: {
      value: "",
      error: "",
      init: false,
    },
    email: {
      value: "",
      error: "",
      init: false,
    },
    phone: {
      value: "",
      error: "",
      init: false,
    },
    password: {
      value: "",
      error: "",
      showPassword: false,
      init: false,
    },
    password2: {
      value: "",
      error: "",
      init: false,
    },
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      console.log(
        values.firstName,
        values.lastName,
        values.email,
        values.password,
        values.phone
      );
      register(
        values.firstName.value,
        values.lastName.value,
        values.email.value,
        values.phone.value,
        values.password.value,
        (response) => {
          console.log(response);
          saveTokenCredentials(response);
          axios.defaults.headers.common.authorization = `Bearer ${
            JSON.parse(localStorage.getItem("token")).access_token
          }`;
          router.push(HOME_PAGE);
        },
        (error) => {
          toast.error(error?.response?.data?.msg, {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: error?.response?.data?.msg,
          });
        }
      );
    } else {
      toast.error("Sprawdź czy wszytskie pola są poprawnie wypełnione", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: "invalidForm",
      });
    }
  };

  const handleChange =
    (prop, error, init = false) =>
    (event) => {
      console.log("Values: ", values, error);
      setValues({
        ...values,
        [prop]: {
          ...values[prop],
          value: event.target.value,
          error: error,
          init: init ? true : values[prop].init,
        },
      });
    };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      password: {
        ...values.password,
        showPassword: !values.password.showPassword,
      },
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const redirectLogin = () => {
    router.push({
      query: { ...query, type: LOGIN_QUERY },
    });
  };

  const redirectReset = () => {
    router.push({
      query: { ...query, type: RESET_PASSWORD_QUERY },
    });
  };

  const isFormValid = () => {
    return !(
      !values.firstName.value! ||
      values.firstName.value.length < 2 ||
      values.firstName.value.length > 64 ||
      !values.lastName.value! ||
      values.lastName.value.length < 2 ||
      values.lastName.value.length > 64 ||
      !values.email.value ||
      checkEmail(values.email.value).length > 0 ||
      !isPossiblePhoneNumber(values.phone.value, "PL") ||
      !values.password.value ||
      values.password.value.length < 8 ||
      values.password.value.length > 128 ||
      !values.password2.value ||
      values.password.value !== values.password2.value
    );
  };

  return (
    <>
      <Typography
        variant="h4"
        sx={{ fontWeight: 300, mt: 5 }}
        align="center"
        gutterBottom
        component="div"
      >
        Zostań Mikołajem już dziś
      </Typography>
      <Box
        sx={AuthBoxStyles}
        component="form"
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <TextField
          label="Imię"
          placeholder="Imię"
          variant="outlined"
          required
          value={values.firstName.value}
          error={values.firstName.init && values.firstName.error.length > 0}
          helperText={values.firstName.init && values.firstName.error}
          onBlur={(e) => {
            handleChange(
              "firstName",
              checkTextLength(e.target.placeholder, 2, 64)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "firstName",
              checkTextLength(e.target.placeholder, 2, 64)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Person />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Nazwisko"
          placeholder="Nazwisko"
          variant="outlined"
          required
          value={values.lastName.value}
          error={values.lastName.init && values.lastName.error.length > 0}
          helperText={values.lastName.init && values.lastName.error}
          onBlur={(e) => {
            handleChange(
              "lastName",
              checkTextLength(e.target.placeholder, 2, 64)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "lastName",
              checkTextLength(e.target.placeholder, 2, 64)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Person />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Email"
          placeholder="Email"
          variant="outlined"
          type="email"
          required
          value={values.email.value}
          error={values.email.init && values.email.error.length > 0}
          helperText={values.email.init && values.email.error}
          onBlur={(e) => {
            handleChange("email", checkEmail(e.target.value), true)(e);
          }}
          onChange={(e) => {
            handleChange("email", checkEmail(e.target.value))(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Email />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Telefon"
          placeholder="Telefon"
          variant="outlined"
          type="phone"
          required
          value={temporaryNumber}
          error={values.phone.init && values.phone.error.length > 0}
          helperText={values.phone.init && values.phone.error}
          onBlur={(e) => {
            try {
              setValues({
                ...values,
                phone: {
                  ...values.phone,
                  value: parsePhoneNumber(
                    e.target.value,
                    "PL"
                  ).number.toString(),
                  error: isPossiblePhoneNumber(e.target.value, "PL")
                    ? ""
                    : "Niepoprawny telefon",
                  init: true,
                },
              });
              setTemporaryNumber(
                parsePhoneNumber(e.target.value, "PL").formatInternational()
              );
            } catch (error) {
              setValues({
                ...values,
                phone: {
                  ...values.phone,
                  error: "Niepoprawny telefon",
                  init: true,
                },
              });
            }
          }}
          onChange={(e) => {
            setTemporaryNumber(new AsYouType("PL").input(e.target.value));
            try {
              setValues({
                ...values,
                phone: {
                  ...values.phone,
                  value: parsePhoneNumber(
                    e.target.value,
                    "PL"
                  ).number.toString(),
                },
              });
            } catch (error) {}
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="phone">
                  <Phone />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Hasło"
          placeholder="Hasło"
          type={values.password.showPassword ? "text" : "password"}
          required
          value={values.password.value}
          error={values.password.init && values.password.error.length > 0}
          helperText={values.password.init && values.password.error}
          onBlur={(e) => {
            handleChange(
              "password",
              checkTextLength(e.target.placeholder, 8, 128)(e),
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "password",
              checkTextLength(e.target.placeholder, 8, 128)(e)
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Lock />
                </IconButton>
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.password.showPassword ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          label="Powtórz hasło"
          placeholder="Powtórz hasło"
          variant="outlined"
          type="password"
          required
          value={values.password2.value}
          error={values.password2.init && values.password2.error.length > 0}
          helperText={values.password2.init && values.password2.error}
          onBlur={(e) => {
            handleChange(
              "password2",
              e.target.value === values.password.value
                ? ""
                : "Hasła nie zgadzają się",
              true
            )(e);
          }}
          onChange={(e) => {
            handleChange(
              "password2",
              e.target.value === values.password.value
                ? ""
                : "Hasła nie zgadzają się"
            )(e);
          }}
          sx={AuthTextFieldStyles}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconButton aria-label="email">
                  <Lock />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <Box>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={AuthButtonStyles}
          >
            Zarejestruj się
          </Button>
        </Box>
        <Box
          sx={{
            // borderBottom: "1px solid",
            // borderColor: "secondary.dark",
            pb: 1,
          }}
        >
          <Typography variant="h6" align="center" gutterBottom component="div">
            Masz już konto?{" "}
            <Link
              onClick={redirectLogin}
              color="primary.dark"
              underline="hover"
              sx={{ cursor: "pointer" }}
            >
              Zaloguj się
            </Link>
          </Typography>
        </Box>
        {/* <Box sx={{ pt: 1 }}>
          <Typography variant="h6" align="center" gutterBottom component="div">
            <Link
              onClick={redirectReset}
              color="primary.dark"
              underline="hover"
              sx={{ cursor: "pointer" }}
            >
              Zapomniałeś hasła?
            </Link>
          </Typography>
        </Box> */}
      </Box>
    </>
  );
};

export default RegisterForm;
