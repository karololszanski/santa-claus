import React from "react";
import Navbar from "../elements/navbar/navbar";

const withNavbar = (Component: any) => {
  return (props) => (
    <>
      <Navbar />
      <Component {...props} />
    </>
  );
};

export default withNavbar;
