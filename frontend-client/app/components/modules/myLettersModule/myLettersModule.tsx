import { Grid, Typography } from "@mui/material";
import { getMyLetters } from "api/letter/getMyLetters";
import useMyLetters from "api/useMyLetters";
import MyLettersTableComponent from "components/elements/myLetters/myLettersTableComponent";
import React, { useEffect, useState } from "react";

const MyLettersModule = () => {
  const { data, error } = useMyLetters();
  const [dataRetrieved, setDataRetrieved] = useState(null);

  useEffect(() => {
    console.log("Nowa data: ", data);
    if (data) {
      setDataRetrieved(data);
    }
  }, [data]);

  return (
    <Grid container spacing={2} mt={8} mb={2} px={2}>
      <Typography m={2} variant="h1" sx={{ fontSize: "36px" }}>
        Moje listy
      </Typography>
      <Grid
        item
        xs={12}
        sx={{
          maxHeight: {
            sm: "calc(100vh - 180px)",
          },
          overflow: "auto",
          mt: 2,
          p: 2,
        }}
      >
        <MyLettersTableComponent data={dataRetrieved} />
      </Grid>
    </Grid>
  );
};

export default MyLettersModule;
