import React, { Fragment, useState } from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Typography from "@mui/material/Typography";
import { StepperBoxStyles } from "./letterModule.style";
import OrphangeCode from "components/elements/letter/orphanageCode";
import SenderDetailsForm from "components/elements/letter/senderDetailsForm";
import LetterForm from "components/elements/letter/letterForm";
import LetterSent from "components/elements/letter/letterSent";
import SummarySendingLetter from "components/elements/letter/summarySendingLetter";

const steps = ["Kod dostępu", "Dane osobowe", "List", "Podsumowanie"];

const LetterModule = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [skipped, setSkipped] = useState(new Set<number>());
  const [letterDetails, setLetterDetails] = useState({
    orphanageCode: "",
    firstName: "",
    lastName: "",
    letterBody: "",
  });

  const isStepSkipped = (step: number) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleSteps = (activeStep) => {
    if (activeStep === 0) {
      return (
        <OrphangeCode
          letterDetails={letterDetails}
          setLetterDetails={setLetterDetails}
          handleNext={handleNext}
        />
      );
    } else if (activeStep === 1) {
      return (
        <SenderDetailsForm
          letterDetails={letterDetails}
          setLetterDetails={setLetterDetails}
          handleBack={handleBack}
          handleNext={handleNext}
        />
      );
    } else if (activeStep === 2) {
      return (
        <LetterForm
          letterDetails={letterDetails}
          setLetterDetails={setLetterDetails}
          handleBack={handleBack}
          handleNext={handleNext}
        />
      );
    } else if (activeStep === 3) {
      return (
        <SummarySendingLetter
          letterDetails={letterDetails}
          handleBack={handleBack}
          handleNext={handleNext}
        />
      );
    } else {
      return <LetterSent handleReset={handleReset} />;
    }
  };

  return (
    <Box>
      <Typography
        variant="h4"
        sx={{ fontWeight: 300, mt: 5, ml: 3, mr: 3 }}
        align="left"
        gutterBottom
        component="div"
      >
        List do świętego Mikołaja
      </Typography>
      <Box sx={StepperBoxStyles}>
        <Stepper activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps: { completed?: boolean } = {};
            if (isStepSkipped(index)) {
              stepProps.completed = false;
            }
            return (
              <Step key={label} {...stepProps}>
                <StepLabel>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>

        <Fragment>{handleSteps(activeStep)}</Fragment>
      </Box>
    </Box>
  );
};

export default LetterModule;
