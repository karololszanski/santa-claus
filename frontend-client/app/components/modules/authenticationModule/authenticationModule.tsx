import router, { useRouter } from "next/dist/client/router";
import React, { useEffect } from "react";
import {
  LOGIN_QUERY,
  REGISTER_QUERY,
  RESET_PASSWORD_QUERY,
} from "../../../constants/navigationConstants";
import LoginForm from "../../elements/authentication/loginForm/loginForm";
import RegisterForm from "../../elements/authentication/registerForm/registerForm";
import ResetPasswordForm from "../../elements/authentication/resetPasswordForm/resetPasswordForm";

const AuthenticationModule = () => {
  const { query } = useRouter();
  const correctTypes = [LOGIN_QUERY, REGISTER_QUERY, RESET_PASSWORD_QUERY];

  useEffect(() => {
    console.log("query: ", { query }, query.type);
    if (
      query.type !== undefined &&
      !correctTypes.includes(query.type.toString())
    ) {
      router.push({
        query: { ...query, type: LOGIN_QUERY },
      });
    }
  }, [query.type]);
  return (
    <>
      {query.type === REGISTER_QUERY ? (
        <RegisterForm />
      ) : query.type === RESET_PASSWORD_QUERY ? (
        <ResetPasswordForm />
      ) : (
        <LoginForm />
      )}
    </>
  );
};

export default AuthenticationModule;
