import { CircularProgress, Grid } from "@mui/material";
import useAllLetters from "api/useAllLetters";
import LettersTableComponent from "components/elements/browser/lettersTableComponent";
import SearchComponent from "components/elements/browser/searchComponent";
import React, { useState } from "react";
import dynamic from "next/dynamic";

const MapLeaflet = dynamic(
  () => import("components/elements/browser/mapComponent"),
  {
    loading: () => <CircularProgress />,
    ssr: false,
  }
);

const MainPageModule = () => {
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedLocation, setSelectedLocation] = useState(null);
  const [localizations, setLocalizations] = useState(null);
  const { data, error } = useAllLetters();

  return (
    <Grid container spacing={2} mt={8} mb={2} px={2}>
      <Grid item xs={12}>
        <SearchComponent
          letters={data}
          selectedItems={selectedItems}
          setSelectedItems={setSelectedItems}
          localizations={localizations}
          setLocalizations={setLocalizations}
          selectedLocation={selectedLocation}
          setSelectedLocation={setSelectedLocation}
        />
      </Grid>

      <Grid
        item
        xs={12}
        sm={7}
        sx={{
          maxHeight: {
            sm: "calc(100vh - 180px)",
          },
          overflow: "auto",
          mt: 2,
          p: 2,
        }}
      >
        <LettersTableComponent
          data={data}
          selectedItems={selectedItems}
          selectedLocation={selectedLocation}
        />
      </Grid>
      <Grid
        item
        sm={5}
        sx={{
          display: { xs: "none", sm: "initial" },
          minHeight: "calc(100vh - 180px)",
          maxHeight: "calc(100vh - 180px)",
        }}
      >
        <MapLeaflet
          localizations={localizations}
          selectedLocation={selectedLocation}
          setSelectedLocation={setSelectedLocation}
        />
      </Grid>
    </Grid>
  );
};

export default MainPageModule;
