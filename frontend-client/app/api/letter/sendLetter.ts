import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function sendLetter(
  first_name,
  last_name,
  code,
  text,
  onSuccess,
  onError
) {
  axios
    .post(`${API_ENDPOINT}/letter`, {
      first_name,
      last_name,
      orphanage_code: code,
      text,
    })
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      console.log("Error 232: ", { error });
      onError(error);
    });
}
