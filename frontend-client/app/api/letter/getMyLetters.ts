import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function getMyLetters(accessToken: string, onSuccess, onError) {
  axios
    .get(`${API_ENDPOINT}/letters`, {
      headers: { authorization: `Bearer ${accessToken}` },
    })
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      onError(error);
    });
}
