import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function connectLetterWithUser(letter_id, onSuccess, onError) {
  axios
    .put(`${API_ENDPOINT}/letter/connect`, {
      letter_id,
    })
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      onError(error);
    });
}
