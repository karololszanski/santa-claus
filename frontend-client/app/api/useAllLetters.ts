import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";
import useSWR from "swr";

// const fetcher = (url) => fetch(url).then((response) => response.json());
const fetcher = (url) => axios.get(url).then((res) => res.data);

const useAllLetters = () => {
  const { data, error } = useSWR(`${API_ENDPOINT}/all_letters`, fetcher, {
    revalidateOnFocus: false,
  });

  return { data, error };
};

export default useAllLetters;
