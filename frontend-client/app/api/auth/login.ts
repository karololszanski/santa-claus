import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function login(email, password, onSuccess, onError) {
  axios
    .post(`${API_ENDPOINT}/user/login`, {
      email: email,
      password: password,
    })
    .then((response) => {
      console.log("Response: ", response);
      onSuccess(response.data);
    })
    .catch((error) => {
      onError(error);
    });
}
