import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function register(
  first_name,
  last_name,
  email,
  phone,
  password,
  onSuccess,
  onError
) {
  axios
    .post(`${API_ENDPOINT}/user`, {
      first_name,
      last_name,
      email,
      phone,
      password,
    })
    .then((response) => {
      console.log("Response: ", response);
      onSuccess(response.data);
    })
    .catch((error) => {
      onError(error);
    });
}
