import axios from "axios";
import { API_ENDPOINT } from "constants/projectConstants";

export function checkOrphanageCode(code: string, onSuccess, onError) {
  axios
    .get(`${API_ENDPOINT}/code/${code}`)
    .then((response) => {
      onSuccess(response.data);
    })
    .catch((error) => {
      onError(error);
    });
}
