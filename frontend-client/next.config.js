const withPlugins = require("next-compose-plugins");

// next.js configuration
const nextConfig = {
  trailingSlash: true,
  target: "serverless",
};

module.exports = withPlugins([], nextConfig);
