import { Grid } from "@mui/material";
import LetterModule from "components/modules/letterModule/letterModule";
import React from "react";

const WriteLetter = () => {
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <LetterModule />
        </Grid>
      </Grid>
    </>
  );
};

export default WriteLetter;
