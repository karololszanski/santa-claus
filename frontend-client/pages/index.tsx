import { Button } from "@mui/material";
import withNavbar from "components/layouts/withNavbar";
import BrowsingLettersModule from "components/modules/browsingLettersModule/browsingLettersModule";
import React, { useState } from "react";

const HomePage = () => {
  const [initialScreen, setInitialScreen] = useState(false);

  return (
    <>
      {initialScreen ? (
        <>
          <div>Welcome to InitialScreen!</div>
          <Button variant="outlined" onClick={() => setInitialScreen(false)}>
            Zostań św. Mikołajem
          </Button>
        </>
      ) : (
        <>
          <BrowsingLettersModule />
        </>
      )}
    </>
  );
};

export default withNavbar(HomePage);
