import { Grid } from "@mui/material";
import React from "react";
import withNavbar from "../app/components/layouts/withNavbar";
import AuthenticationModule from "../app/components/modules/authenticationModule/authenticationModule";

const Auth = () => {
  return (
    <>
      <Grid container spacing={2} mt={8}>
        <Grid item xs={12}>
          <AuthenticationModule />
        </Grid>
        {/* <Grid item xs={12} sm={6}>
          <AuthenticationModule />
        </Grid>
        <Grid item xs={12} sm={6}>
          <h1>TEst</h1>
        </Grid> */}
      </Grid>
    </>
  );
};

export default withNavbar(Auth);
