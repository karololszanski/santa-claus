import { ThemeProvider } from "@mui/material/styles";
import React from "react";
import theme from "../app/styles/default";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Auth from "utils/Auth";

//Fonts
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "@fontsource/caveat/400.css";

export default function ExtendedApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <ToastContainer />
      <Auth />
      <Component {...pageProps} />
    </ThemeProvider>
  );
}
