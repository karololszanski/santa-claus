import withNavbar from "components/layouts/withNavbar";
import MyLettersModule from "components/modules/myLettersModule/myLettersModule";
import React from "react";

const MyLettersPage = () => {
  return (
    <>
      <MyLettersModule />
    </>
  );
};

export default withNavbar(MyLettersPage);
